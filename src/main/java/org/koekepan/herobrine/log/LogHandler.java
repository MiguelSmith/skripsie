package org.koekepan.herobrine.log;

import java.io.IOException;

import org.koekepan.herobrine.log.io.LogInput;
import org.koekepan.herobrine.log.io.LogOutput;

public interface LogHandler {
	
	public void register(Integer id, Class<? extends Log> type);
	public void write(LogOutput out, Log log) throws IOException;
	public Log read(LogInput in) throws IOException;
	
}
