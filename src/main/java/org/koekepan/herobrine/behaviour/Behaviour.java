package org.koekepan.herobrine.behaviour;

public interface Behaviour <T> {
	public void process(T object);
}
