package org.koekepan.herobrine.command;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.behaviour.BehaviourHandler;
import org.koekepan.herobrine.command.commands.Command;

public class CommandHandler implements CommandInterface, Runnable {

	private BehaviourHandler<Command> commandBehaviours;
	private Command command;

	public CommandHandler(BehaviourHandler<Command> commandBehaviours) {
		this.commandBehaviours = commandBehaviours;
		command = null;
	}

	@Override
	public Command getCommand() {
		return command;
	}

	@Override
	public void setCommand(Command command) {
		this.command = command;
	}

	@Override
	public boolean needsCommand() {
		return command == null || command.isComplete();
	}

	@Override
	public void clearCommand() {
		command = null;
	}

	@Override
	public void run() {	

		if(command != null) {
			if(command.isComplete()) {
				command = null;
			} else {
				commandBehaviours.process(command);
			}	
		}
	}

}