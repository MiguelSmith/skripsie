package org.koekepan.herobrine.command;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.client.ClientControl;
import org.koekepan.herobrine.client.packet.PacketListener;
import org.koekepan.herobrine.client.packet.PacketSession;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.command.behaviours.CommandBehaviours;

public class CommandSystem implements ClientControl {

	private ClientCommandControl control;
	private CommandHandler commandHandler;
	
	public CommandSystem(ClientCommandControl control) {
		this.control = control;
	}

	@Override
	public void setup(String name, int interval, PacketSession session, ClientState state) {
		commandHandler = new CommandHandler(new CommandBehaviours(interval, session, state));
		control.setup(name, interval, commandHandler, state, session);
	}
	
	@Override
	public PacketListener getPacketListener() {
		return control.getPacketListener();
	}
	
	public void addWaypoint(int x, int y, int z) {
		control.addWaypoint(x, y, z);
	}

	@Override
	public void run() {
		control.run();
		commandHandler.run();
	}

}
