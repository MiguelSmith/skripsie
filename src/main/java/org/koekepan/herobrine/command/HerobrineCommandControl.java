package org.koekepan.herobrine.command;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.client.packet.PacketListener;
import org.koekepan.herobrine.client.packet.PacketSession;
import org.koekepan.herobrine.client.pathfinding.Path;
import org.koekepan.herobrine.client.pathfinding.WaypointChecker;
import org.koekepan.herobrine.client.pathfinding.AStar.AStarAlgorithm;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.command.commands.Command;
import org.koekepan.herobrine.command.commands.movement.JumpCommand;
import org.koekepan.herobrine.command.commands.movement.MoveCommand;

import org.spacehq.mc.protocol.data.game.Position;

public class HerobrineCommandControl implements ClientCommandControl {

	private String name = "herobrine";
	private CommandInterface commandInterface = null;
	private ClientState state = null;
	private PacketSession session = null;
	private WaypointChecker waypointChecker = null;
	private AStarAlgorithm pathfinder = null;
	private Path path = null;
	private Command command = null;

	private ScheduledExecutorService pathExecutor;

	int index = 0;
	int destination = -1;
	int numDestinations = 0;
	List<Command> waypoints = new LinkedList <Command>();

	boolean columnsInitialized = false;
	boolean pathInitialized = false;
	boolean waypointMode = false;
	boolean waypointAdded = false;
	boolean waypointsInitialized = false;
	boolean readyForCommand = false;

	int x = 0;
	int y = 4;
	int z = 0;
	int r = 0;

	int[][] points = null;
	LinkedList<Position> destinations = new LinkedList<Position>();
	Position destinationPos;

	public HerobrineCommandControl(int x , int z, int r) {
		this.x = x;
		this.z = z;
		this.r = r;
		waypointMode = false;
	}

	public HerobrineCommandControl(int[][] points) {
		for(int i=0;i<points.length;i++) {
			destinations.add(new Position(points[i][0], points[i][1], points[i][2]));
		}

		this.points = points;
		numDestinations = this.points.length;
		waypointMode = true;
	}


	@Override
	public void setup(String name, int interval, CommandInterface commandInterface, ClientState state, PacketSession session) {
		this.name = name;
		this.commandInterface = commandInterface;
		this.state = state;
		this.session = session;

		if (waypointMode) {
			waypointChecker = new WaypointChecker(points, this.state);
		} else {
			waypointChecker = new WaypointChecker(x,y,z, this.state);
		}


		pathfinder = new AStarAlgorithm(200, state);//TODO look at ways to define path length
	}

	@Override
	public void run() {		
		//Load chunks
		if(!columnsInitialized) {
			ConsoleIO.updateln("Loading chunks..." + ((float)state.getWorldState().countColumns()/441)*100+"%");
			if(state.getWorldState().areColumnsLoaded()) {
				ConsoleIO.println("Loading chunks... 100% Complete!");
				columnsInitialized = true;
			}
		}

		//reset 
		if (waypointAdded && !readyForCommand) {
			pathInitialized = false;
			pathfinder.setDoneToFalse();
			waypointsInitialized = false;
			waypointAdded = false;
		}

		if (columnsInitialized && !pathInitialized && !destinations.isEmpty()) {
			pathInitialized = true;				
			pathExecutor = Executors.newSingleThreadScheduledExecutor();

			pathExecutor.schedule((new Runnable() {
				@Override
				public void run() {
					destinationPos = destinations.remove();
					ConsoleIO.println("Path-finding to ["+destinationPos.getX()+","+destinationPos.getY()+","+destinationPos.getZ()+"] starting");
					destination++;
					path = pathfinder.findPath(state.getPlayerState().getX(), state.getPlayerState().getY(), state.getPlayerState().getZ(), destinationPos.getX(), destinationPos.getY(), destinationPos.getZ(), destination);
					while (!pathfinder.isDone()) {
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							Thread.currentThread().interrupt();
						}
					}
				}
			}), 0, TimeUnit.MILLISECONDS);
		}

		if (pathfinder.isDone()) {
			if (path != null) {
				waypointsInitialized = true;
				ConsoleIO.println("Player \""+this.name+"\" has started moving to the next destination: <"+destinationPos.getX()+","+destinationPos.getY()+","+destinationPos.getZ()+">");
			} else if (destination!=(numDestinations-1)){
				pathInitialized = false;
			}
			pathExecutor.shutdown();
			pathfinder.setDoneToFalse();
		}

		if (waypointsInitialized) {
			waypoints.clear();
			for (int i=0; i<path.getLength();i++) {
				if (path.getType(i)) {
					//ConsoleIO.println(path.getStep(i).toString());
					waypoints.add(new MoveCommand(path.getX(i), path.getY(i), path.getZ(i)));
					if (i<path.getLength()-1) {
						//ConsoleIO.println(path.getStep(i+1).toString());
						waypoints.add(new JumpCommand(path.getX(i+1), path.getY(i+1), path.getZ(i+1), path.getJumpDirection(i+1)));
					}
				} else {
					//ConsoleIO.println(path.getStep(i).toString());
					waypoints.add(new MoveCommand(path.getX(i), path.getY(i), path.getZ(i)));
				}
			}
			waypointsInitialized = false;
			readyForCommand = true;
		}

		// give next command in sequence
		if(commandInterface.needsCommand() && readyForCommand) {
			try {
				command = waypoints.get(index);
			} catch (ArrayIndexOutOfBoundsException e) {
				command = null;
			}

			if (command.isComplete()) {
				if(index == (waypoints.size()-1)) {
					ConsoleIO.println("Player \""+this.name+"\" has reached waypoint: " + command.toString());
					if (destinations.isEmpty()) {
						readyForCommand = false;
						waypoints.clear();
						command = null;
					} else {
						pathInitialized = false;
						readyForCommand = false;
						command = null;
					}
					index = 0;
				} else {
					index++;
					command = waypoints.get(index);
					command.setComplete(false);
				}				
			} 
			commandInterface.setCommand(command);
		}
	}

	public boolean setCurrentWaypoint(int index) {
		boolean result = false;
		if ((index < waypoints.size()) && (index >= 0)) {
			this.index = index;
			result = true;
		}
		return result;
	}


	public int getCurrentWaypoint() {
		return this.index;
	}

	public void addWaypoint(int x, int y, int z) {
		WaypointChecker destination = new WaypointChecker(x, y, z, state);

		if (!destination.isValid()) {
			ConsoleIO.println("Destination ["+x+","+y+","+z+"] is not a valid destination");
		} else {
			Position newDest = new Position(x, y, z);
			destinations.add(newDest);

			numDestinations++;
			waypointAdded = true;
			ConsoleIO.println("New destination added at ["+x+","+y+","+z+"]");
		}
	}

	@Override
	public PacketListener getPacketListener() {
		return null; // TODO Huh?
	}

}
