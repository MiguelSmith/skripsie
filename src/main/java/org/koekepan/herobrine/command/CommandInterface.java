package org.koekepan.herobrine.command;

import org.koekepan.herobrine.command.commands.Command;

public interface CommandInterface {
	public  Command getCommand();
	public  void setCommand(Command command);
	public  boolean needsCommand();
	public  void clearCommand();
}
