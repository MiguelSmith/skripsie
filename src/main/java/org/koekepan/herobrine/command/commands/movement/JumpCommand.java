package org.koekepan.herobrine.command.commands.movement;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.command.commands.Command;

public class JumpCommand extends Command {
	
	private double tx;
	private double ty;
	private double tz;
	private boolean up;
	
	// absolute movement
	public JumpCommand(double tx, double ty, double tz, boolean up) {
		this.tx = tx;
		this.ty = ty;
		this.tz = tz;
		this.up = up;
		//ConsoleIO.println("Jump Command "+toString());
	}
	
	public void setTx(double x) {
		this.tx = x;
	}
	
	public void setTy(double y) {
		this.ty = y;
	}
	
	public void setTz(double z) {
		this.tz = z;
	}
	
	public double getTx() {
		return tx;
	}
	
	public double getTy() {
		return ty;
	}
	
	public double getTz() {
		return tz;
	}
	
	public boolean getJump() {
		return up;
	}
	
	@Override
	public String toString() {
		return "to <"+getTx()+","+getTy()+","+getTz()+">";
	}
	
}