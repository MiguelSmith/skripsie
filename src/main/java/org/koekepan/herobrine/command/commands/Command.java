package org.koekepan.herobrine.command.commands;

import org.koekepan.herobrine.command.CommandCallback;

public abstract class Command {

	private CommandCallback callback;
	private boolean complete;

	public Command() {
		this(null);
	}

	public Command(CommandCallback callback) {
		this.callback = callback;
		complete = false;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) { // TODO fix the api
		this.complete = complete;
	}

	public void callback() {
		if(callback != null) {
			callback.callback();
		}
	}

}
