package org.koekepan.herobrine.command.commands.movement;

import org.koekepan.herobrine.client.state.entity.Entity;
import org.koekepan.herobrine.command.commands.Command;

public class MoveCommand extends Command {
	
	private double x;
	private double y;
	private double z;
	
	// absolute movement
	public MoveCommand(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	// relative movement
	public MoveCommand(double x, double y, double z, Entity entity) {
		this.x = entity.getX() + x;
		this.y = entity.getY() + y;
		this.z = entity.getZ() + z;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public void setZ(double z) {
		this.z = z;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}

	
	@Override
	public String toString() {
		return "<"+getX()+","+getY()+","+getZ()+">";
	}
	
}