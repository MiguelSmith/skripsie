package org.koekepan.herobrine.command.behaviours;

import org.koekepan.herobrine.behaviour.BehaviourHandler;
import org.koekepan.herobrine.client.packet.PacketSession;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.command.behaviours.movement.JumpCommandBehaviour;
import org.koekepan.herobrine.command.behaviours.movement.MoveCommandBehaviour;
import org.koekepan.herobrine.command.commands.Command;
import org.koekepan.herobrine.command.commands.movement.JumpCommand;
import org.koekepan.herobrine.command.commands.movement.MoveCommand;

public class CommandBehaviours extends BehaviourHandler<Command> {
	
	private int interval;
	private PacketSession session;
	private ClientState state;
	
	public CommandBehaviours(int interval, PacketSession session, ClientState state) {
		
		this.interval = interval;
		this.session = session;
		this.state = state;
		
		registerDefaultBehaviours();
	}
	
	public void registerDefaultBehaviours() {
		
		clearBehaviours();
		
		registerBehaviour(MoveCommand.class, new MoveCommandBehaviour(interval, session, state));
		registerBehaviour(JumpCommand.class, new JumpCommandBehaviour(interval, session, state));
	}

}
