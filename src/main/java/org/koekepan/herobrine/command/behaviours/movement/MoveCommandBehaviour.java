package org.koekepan.herobrine.command.behaviours.movement;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.command.commands.Command;
import org.koekepan.herobrine.command.commands.movement.MoveCommand;
import org.koekepan.herobrine.client.packet.PacketSession;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.player.PlayerState;
import org.spacehq.mc.protocol.packet.ingame.client.player.ClientPlayerPositionRotationPacket;
import org.spacehq.packetlib.packet.Packet;

public class MoveCommandBehaviour implements Behaviour<Command> {

	private final static double SPEED = 43.17; 

	private int interval;
	private PacketSession session;
	private ClientState state;
	
	private int tickCount = 0;

	public MoveCommandBehaviour(int interval, PacketSession session, ClientState state) {
		this.interval = interval;
		this.session = session;
		this.state = state;
	}

	@Override
	public void process(Command command) {
		
		if(!command.isComplete()) {
			MoveCommand move = (MoveCommand) command;

			PlayerState player = state.getPlayerState();

			double dx = move.getX() - player.getX();
			double dy = move.getY() - player.getY();
			double dz = move.getZ() - player.getZ();
			double ds = Math.sqrt(dx*dx+ dy*dy + dz*dz);
			
			if(ds > 0.20 && tickCount<10) { 	
				tickCount++;

				double moveSpeed = SPEED*state.getPlayerState().getAbilities().getWalkSpeed();
				double k = (interval/1000.0) * (moveSpeed/ds)*1.1;
				double x = player.getX() + dx*k;
				double y = player.getY() + dy*k;
				double z = player.getZ() + dz*k;
				float yaw = (float) Math.toDegrees(Math.atan2(-dx, dz));

				Packet out = new ClientPlayerPositionRotationPacket(true, x, y, z, yaw, 0);
				session.send(out);

			} else {
				tickCount=0;
				command.setComplete(true);
			}	
		}
	}

}
