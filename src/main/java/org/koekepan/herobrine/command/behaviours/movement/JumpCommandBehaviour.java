package org.koekepan.herobrine.command.behaviours.movement;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.command.commands.Command;
import org.koekepan.herobrine.command.commands.movement.JumpCommand;
import org.koekepan.herobrine.client.packet.PacketSession;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.player.PlayerState;
import org.spacehq.mc.protocol.packet.ingame.client.player.ClientPlayerPositionRotationPacket;
import org.spacehq.packetlib.packet.Packet;

public class JumpCommandBehaviour implements Behaviour<Command> {

	private final static double SPEED = 43.17;
	private final static double GRAVITY = 0.16;

	private double velY = 0.539625;
	private double velX = 0.21585;
	private double velXDown = 0.11;

	private boolean peak = false;

	private double a = -0.025551020408163265306;
	private double b = 0.357714285714;
	private double a2 = -(8.0/25);
	private double b2 = 7.0/5;


	private int jumpTick = 0;

	private int interval;
	private PacketSession session;
	private ClientState state;

	public JumpCommandBehaviour(int interval, PacketSession session, ClientState state) {
		this.interval = interval;
		this.session = session;
		this.state = state;
	}

	public void process(Command command) {
		if (!command.isComplete()) {
			jumpTick++;
			JumpCommand jump = (JumpCommand) command;

			PlayerState player = state.getPlayerState();

			double dx = jump.getTx() - player.getX();
			double dy = jump.getTy() - player.getY();
			double dz = jump.getTz() - player.getZ();
			double ds = Math.sqrt(dx*dx+dz*dz);

			double x = player.getX();
			double y = player.getY();
			double z = player.getZ();
			
			double moveSpeed = SPEED*state.getPlayerState().getAbilities().getWalkSpeed();
			double k = (interval/1000.0) * (moveSpeed/ds)*1.1;

			boolean jumpUp = jump.getJump();

			float yaw;

			//ConsoleIO.println("JumpTick:"+jumpTick+" Time:"+time+" JumpUp:"+jumpUp);

			if (ds>0.2 && jumpTick<20) {
				if (jumpUp) {
					x=(Math.abs(dx)>0.1)?(dx>0)?x+velX:x-velX:x;
					if (peak){
						y = jump.getTy();
					} else {
						y = y+a*jumpTick*jumpTick+b*jumpTick;

						if (y>jump.getTy()) {
							peak = true;
						}
					}
					z=(Math.abs(dz)>0.1)?(dz>0)?z+velX:z-velX:z;
					yaw = (float) Math.toDegrees(Math.atan2(-dx, dz));
				} else {
					x=(Math.abs(dx)>0.1)?x+dx*k:x;
					
					if ((Math.abs(dx)>0.75)&&(Math.abs(dz)>0.75)||(Math.abs(dx)>0.4)||(Math.abs(dz)>0.4)) {
						y = player.getY();
					} else {
						y = jump.getTy();
					}
					//ConsoleIO.println("k="+k+" Math.abs(dz):"+(Math.abs(dz)>0.1));
					z=(Math.abs(dz)>0.1)?z+dz*k:z;
					yaw = (float) Math.toDegrees(Math.atan2(-dx, dz));
				}
				
				//ConsoleIO.println("dx:"+dx+" dy:"+dy+" dz:"+dz);
				//ConsoleIO.println("X="+x+" Y="+y+" Z="+z);
				Packet out = new ClientPlayerPositionRotationPacket(false, x, y, z, yaw, 0);
				session.send(out);
			} else {
				peak = false;
				jumpTick=0;
				command.setComplete(true);
			}
		}
	}
}