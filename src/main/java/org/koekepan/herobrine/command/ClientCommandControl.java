package org.koekepan.herobrine.command;

import org.koekepan.herobrine.client.packet.PacketListener;
import org.koekepan.herobrine.client.packet.PacketSession;
import org.koekepan.herobrine.client.state.ClientState;

public interface ClientCommandControl extends Runnable {
	public void setup(String name, int interval, CommandInterface commandInterface, ClientState state, PacketSession session);
	public void addWaypoint(int x, int y, int z);
	public PacketListener getPacketListener();
}
