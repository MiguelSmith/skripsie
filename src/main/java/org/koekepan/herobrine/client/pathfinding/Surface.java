package org.koekepan.herobrine.client.pathfinding;

import java.io.Serializable;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.client.pathfinding.heuristic.Euclidean;
import org.koekepan.herobrine.client.pathfinding.heuristic.Heuristic;

public class Surface implements Comparable<Surface>{
	private int x;
	private int y;
	private int z;
	private int id;
	private int depth;
	
	private int pathID;
	
	private double heuristic;
	private double cost;
	
	private Surface parent;

	private Heuristic euclidean;
	
	public Surface() {
		this.depth = 0;
		this.cost = 0;
		this.pathID = -1;
	}
	
	public Surface(int x, int y, int z, int id, int pathID) {
		this.x = x;
		this.y = y;
		this.z = z;
		
		this.pathID = pathID;
		
		this.depth = 0;
		this.cost = 0;
		
		this.id = id;		
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public int getZ() {
		return this.z;
	}
	
	public int getId() {
		return this.id;
	}
	
	public double getCost() {
		return cost;
	}
	
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public int getDepth() {
		return depth;
	}
	
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	public void setHeuristic(int xp, int yp, int zp, int tx, int ty, int tz, Heuristic heuristic) {
		//TODO create generic heuristic cost generator
		euclidean = heuristic;
		this.heuristic = euclidean.calculate(xp, yp, zp, tx, ty, tz);
	}
	
	public double getHeuristic() {
		return heuristic;
	}
	
	public Surface getParent() {
		return parent;
	}
	
	public int setParent(Surface parent) {
		depth = parent.getDepth() + 1;
		
		this.parent = parent;
		
		return depth;
	}
	
	public boolean comparePathID(int pathID) {
		return (this.pathID==pathID)? true:false;
	}
	
	public int getPathID() {
		return pathID;
	}
	
	public void setPathID(int pathID) {
		this.pathID = pathID;
	}
	
	@Override
	public int compareTo(Surface other) {
		
		double f = heuristic + cost;
		double of = other.heuristic + other.cost;
		
		if (f<of) {
			return -1;
		}else if (f>of) {
			return 1;
		} else {
			return 0;
		}
	}
	
	public void reset() {
		this.cost = 0;
		this.depth = 0;
		
		this.parent = null;
	}
	
	public boolean equals(Surface e) {
		return ((e.getX()==x) && (e.getY()==y) && (e.getZ()==z)) ? true:false;
	}
	
	public String toString() {
		return "Node: [" +x+","+y+","+z+"] BlockID: "+id+" Cost: "+cost+ " Heuristic: "+heuristic;
	}
	
	
}