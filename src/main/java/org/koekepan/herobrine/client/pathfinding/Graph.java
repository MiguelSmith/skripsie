package org.koekepan.herobrine.client.pathfinding;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.client.state.world.ColumnKey;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

public class Graph{
	private ConcurrentHashMap<ColumnKey, Surface> graph;	
	
	public Graph(){
		graph = new ConcurrentHashMap<ColumnKey, Surface>();
	}
	
	public void setSurface(Surface surface, int pathID) {
		surface.setPathID(pathID);
		graph.put(new ColumnKey(surface.getX(), surface.getZ()), surface);
	}
	
	public void setSurface(ColumnKey key, int y, int id, int pathID) {
		Surface surface = new Surface(key.getX(), y, key.getZ(), id, pathID);
		graph.put(key, surface);
	}
	
	public Surface getSurface(int x, int z) {
		ColumnKey key = new ColumnKey(x,z);
		
		return graph.get(key);
	}
}