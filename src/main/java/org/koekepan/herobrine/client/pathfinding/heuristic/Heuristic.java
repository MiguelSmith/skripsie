package org.koekepan.herobrine.client.pathfinding.heuristic;

public interface Heuristic{
	public double calculate(int sx, int sy, int sz, int tx, int ty, int tz);
}