package org.koekepan.herobrine.client.pathfinding;

import org.koekepan.herobrine.client.state.world.WorldState;
import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.world.Block;
import java.util.List;
import java.util.ArrayList;

public class WaypointChecker {
	private int x;
	private int y;
	private int z;
	private ClientState state = null;	
	private WorldState worldState = null;
	int[][] waypoints = null;
	private List<Block> block = new ArrayList<Block>();

	public WaypointChecker(int x, int y, int z, ClientState state) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.state = state;

		this.worldState = this.state.getWorldState();		
	}

	public WaypointChecker(int[][] points, ClientState state) {
		this.waypoints = points;
		this.state = state;

		this.worldState = state.getWorldState();
	}

	public boolean isValid() {
		boolean valid = isIntermediateValid();

		return valid;
	}

	public boolean isIntermediateValid() {
		boolean valid = false;
		try {
			block.add(worldState.getBlock(x, y, z));
			block.add(worldState.getBlock(x, y+1, z));
			block.add(worldState.getBlock(x, y-1, z));
			int id0 = block.get(0).getId();
			int id1 = block.get(1).getId();
			int id2 = block.get(2).getId();
			/*	In order to get the client moving in a random environment, one has to ensure that the blocks checked
			 * 	are of the right types. Further implementation would require a list of types that the client can and
			 * 	cannot pass through and checking block ids against those. Basic ones are added here to give an idea 
			 * 	of what to place.
			 * */
			if ((id0!=0 && id0!=171&&id0!=31&&id0!=38) || id1!=0 || id2==0||id2==171||id2==31||id2==38) {//add id2==x for avoidance of infinite path where x is a traversable block
				valid = false;
			} else {
				valid = true;
			}
		} catch (NullPointerException e) {
			try {
				if (block.get(0).getId()!=0 && block.get(0).getId()!=171&& block.get(0).getId()!=31&& block.get(0).getId()!=38){
					valid = false;
					block.clear();
					return valid;
				} else {
					valid = true;
				}
			} catch (NullPointerException e2) {
				valid = false;
				block.clear();
				return valid;
			}
			try {
				if (block.get(1).getId()!=0){//look at blocks that are traversable but can hang in the air ie vines
					valid = false;
					block.clear();
					return valid;
				}
			} catch (NullPointerException e2) {
				//do nothing else
			}
			try {
				if (block.get(2).getId()==0||block.get(2).getId()==171||block.get(2).getId()==31||block.get(2).getId()==38) {//add block types for different types of floor other than grass block
					valid = false;
				} else {
					valid = true;
				}
			} catch (NullPointerException e2) {
			}
		}

		block.clear();
		return valid;
	}
}