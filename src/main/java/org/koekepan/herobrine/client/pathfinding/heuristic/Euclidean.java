package org.koekepan.herobrine.client.pathfinding.heuristic;

public class Euclidean implements Heuristic {
	public Euclidean() {		
	}
	
	public double calculate(int sx, int sy, int sz, int tx, int ty, int tz) {
		float dx = Math.abs(sx-tx);
		float dy = Math.abs(ty-sy);
		float dz = Math.abs(sz-tz);
		
		return 10*Math.sqrt(dx*dx+dy*dy+dz*dz);
	}
}