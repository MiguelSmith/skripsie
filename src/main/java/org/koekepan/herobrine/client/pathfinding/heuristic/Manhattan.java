package org.koekepan.herobrine.client.pathfinding.heuristic;

import java.io.Serializable;

public class Manhattan implements Heuristic, Serializable {
	public double calculate(int sx, int sy, int sz, int tx, int ty, int tz) {
		float dx = Math.abs(sx-tx);
		float dy = Math.abs(ty-sy);
		float dz = Math.abs(sz-tz);
		
		return 10*(dx+dy+dz);
	}
}