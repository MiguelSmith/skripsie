package org.koekepan.herobrine.client.pathfinding.AStar;

import java.util.ArrayList;
import java.util.PriorityQueue;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.client.pathfinding.Graph;
import org.koekepan.herobrine.client.pathfinding.heuristic.Euclidean;
import org.koekepan.herobrine.client.pathfinding.heuristic.Heuristic;
import org.koekepan.herobrine.client.pathfinding.Path;
import org.koekepan.herobrine.client.pathfinding.Surface;
import org.koekepan.herobrine.client.pathfinding.WaypointChecker;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.world.WorldState;

public class AStarAlgorithm {
	private ArrayList<Surface> closed = new ArrayList<Surface>();
	private PriorityQueue<Surface> open = new PriorityQueue<Surface>();

	private int maxSearchDistance;
	private Surface current = null;
	private Graph graph;
	private Heuristic heuristic;
	private int sx;
	private int sy;
	private int sz;

	private int pathID;

	private int width;//TODO implement width checker
	private int length;

	private boolean done = false;

	private Surface surface;
	private ClientState state;
	private WorldState world;
	private WaypointChecker waypoint;

	public AStarAlgorithm (int maxSearchDistance, ClientState state) {//TODO add heuristic option
		this(maxSearchDistance, state, new Euclidean(),336,336); //336 is the default block length and width
	}

	public AStarAlgorithm(int maxSearchDistance, ClientState state, Heuristic heuristic, int width, int length) {

		this.maxSearchDistance = maxSearchDistance;
		this.heuristic = heuristic;
		this.state = state;

		this.world = state.getWorldState();

		this.graph = world.getGraph();

		this.length = length;
		this.width = width;
	}

	public Path findPath(double sx, double sy, double sz, int tx, int ty, int tz, int pathID) {
		long startTime = System.nanoTime();

		done = false;
		current = null;

		this.sx = (int)Math.floor(sx);
		this.sy = (int)Math.floor(sy);
		this.sz = (int)Math.floor(sz);

		surface = world.setSurfaceBlock(this.sx, this.sy, this.sz, pathID);
		surface.setCost(0);
		//ConsoleIO.println(surface.toString());

		surface.setHeuristic(this.sx, this.sy, this.sz, tx, ty, tz, heuristic);
		surface.setDepth(0);

		closed.clear();
		open.clear();

		addToOpen(surface);

		//ConsoleIO.println("Target X"+tx+", Target Z="+tz);
		Surface target = world.setSurfaceBlock(tx, ty, tz, pathID);
		Surface empty = new Surface();

		waypoint = new WaypointChecker(tx, ty, tz, state);

		if (!waypoint.isValid()) {
			ConsoleIO.println("Destination ["+tx+","+ty+","+tz+"] is not a valid destination");
			done = true;
			return null;
		}

		if (surface.equals(target)){
			ConsoleIO.println("Client is already at destination ["+tx+","+ty+","+tz+"]");
			done = true;
			return null;
		}

		target.setParent(empty);

		int maxDepth = 0;
		double gCost = 0;
		int lx;
		int ly;
		int lz;		

		long medianTime = 0;
		//ConsoleIO.println("Just before while + "+maxDepth+" + "+maxSearchDistance+" + "+open.size());
		while((maxDepth<maxSearchDistance) && (open.size()!=0) && (medianTime<10000)) {
			medianTime = System.nanoTime();
			medianTime = (medianTime-startTime)/1000000;
			current = getFirstInOpen();

			lx = current.getX();
			ly = current.getY();
			lz = current.getZ();
			maxDepth = current.getDepth();

			//ConsoleIO.println("sx="+lx+" sz="+lz);
			//ConsoleIO.println("Current = "+current.toString());

			if (current.equals(target)) {
				//ConsoleIO.println(target.toString());
				//ConsoleIO.println(current.toString());
				target = current;
				break;
			}

			for (int x=-1;x<2;x++) {
				for (int y=-1;y<2;y++) {
					for (int z=-1;z<2;z++) {
						if ((x==0) && (z==0)) {
							continue;
						}

						int xp = x + lx;
						int yp = y + ly;
						int zp = z + lz;
						//ConsoleIO.println("x="+xp+" y="+yp+" z="+zp+" pathID:"+pathID);

						waypoint = new WaypointChecker(xp, yp, zp, state);
						if (waypoint.isIntermediateValid()) {
							gCost = current.getCost() + getMovementCost(lx, ly, lz, xp, yp, zp); // cost is the path cost to this point

							Surface neighbour = world.setSurfaceBlock(xp, yp, zp, pathID);
							
							if (!isValidMove(lx,ly,lz,xp,yp,zp)){
								gCost = gCost*1E4;
								neighbour.setCost(gCost);
								//world.updateGraph(neighbour, pathID);
								addToClosed(neighbour);
							}
							
							//ConsoleIO.println("gCost: "+gCost+" Neighbour cost:"+neighbour.getCost());
							if (gCost < neighbour.getCost()) {
								if (inOpenList(neighbour)) {
									removeOpenList(neighbour);
								}
								if (inClosedList(neighbour)) {
									removeClosedList(neighbour);
								}
							}
							
							if (inClosedList(neighbour)) {
								//ConsoleIO.println("In closed list: "+neighbour.toString());
								continue;
							}
							
							if (!inOpenList(neighbour)) {
								neighbour.setCost(gCost);
								neighbour.setHeuristic(xp, yp, zp, tx, ty, tz, heuristic);
								//ConsoleIO.println("x="+xp+" y="+yp+" z="+zp + " Cost: "+gCost+" Heuristic: "+neighbour.getHeuristic());
								neighbour.setParent(current);
								//ConsoleIO.println("Set parent "+neighbour.getParent().toString()+" with the current node being "+neighbour.toString());
								maxDepth = Math.max(maxDepth, neighbour.setParent(current));
								//ConsoleIO.println("Set parent after maxDepth "+neighbour.getParent().toString()+" with the current node being "+neighbour.toString());
								neighbour.setDepth(maxDepth);
								//world.updateGraph(neighbour, pathID);
								addToOpen(neighbour);
								//ConsoleIO.println("add to Open "+neighbour.toString());
							}

						}
					}
				}
			}
			addToClosed(current);
			//ConsoleIO.println("Add to Closed "+current.toString());
		}

		if (target.getDepth() == 1 && !(target.getParent().equals(surface))) {
			done = true;
			long endtime = System.nanoTime();
			long duration = (endtime-startTime)/1000000;
			ConsoleIO.println("Path could not be found to destination ["+tx+","+ty+","+tz+"] in "+duration+"ms");
			return null;
		}
		
		if (maxDepth==maxSearchDistance) {
			done = true;
			long endtime = System.nanoTime();
			long duration = (endtime-startTime)/1000000;
			ConsoleIO.println("Path could not be found to destination ["+tx+","+ty+","+tz+"] in "+duration+"ms due to max search distance being reached ("+maxSearchDistance+")");
			return null;
		}

		Path path = new Path(tx,ty,tz, world.getGraph());
		int pathLength = 0;
		
		while (target != surface) {
			//ConsoleIO.println(target.toString());
			path.prependStep(target.getX(), target.getY(), target.getZ(), pathLength);
			target = target.getParent();
			pathLength++;
		}
		path.prependStep(this.sx, this.sy, this.sz, pathLength); //not sure of this is needed
		//ConsoleIO.println(path.toString());

		done = true;
		long endTime = System.nanoTime();

		long duration = (endTime - startTime)/1000000; 		
		ConsoleIO.println("Pathfinding done in "+duration+"ms!");

		return path;
	}

	private void addToOpen(Surface node) {
		try {
			if (!inOpenList(node)){
				open.add(node);	
			}
			//ConsoleIO.println("Added to open:"+node.toString());
		} catch (ClassCastException | NullPointerException e) {
			ConsoleIO.println("Add node to open list error + "+e);
		}
	}

	private void addToClosed(Surface node) {
		try {
			if (!inClosedList(node)) {
				closed.add(node);
			}
			//ConsoleIO.println("Added to closed:"+node.toString());
		} catch (ClassCastException | NullPointerException e) {
			ConsoleIO.println("Add node to closed list error +"+e);
		}
	}

	private Surface getFirstInOpen() {
		return open.poll();
	}

	private double getMovementCost(int lx, int ly, int lz, int xp, int yp, int zp) {
		if ((xp-lx != 0) && (zp-lz != 0)) {//diagonal
			if (yp-ly>0.5) {
				return 14;	//jump up
			} else if (yp-ly<-0.5) {
				return 14;	//jump down			
			}else {
				return 14;	//same level				
			}
		}
		if (yp-ly>0.5) {
			return 10; 	//straight or sideways jump up
		} else if (yp-ly<-0.5) {
			return 10;	//straight or sideways jump down
		}else {
			return 10;	//normal walk sideways or straight
		}
	}

	private boolean isValidMove(int sx, int sy, int sz, int tx, int ty, int tz) {
		int id;
		try {
			id = world.getBlock(tx,ty,tz).getId();
		} catch (NullPointerException e) {
			id = 0;
		}
		if (id==0|| id==171||id==31||id==38) {//add ids here if the block is traversable
			if ((tx-sx!=0)&&(tz-sz!=0)) {//if diagonal movement then check to make sure diagonal movement is possible
				if ((tx-sx>0)&&(tz-sz>0)) {
					return checkDiagonalBlocks(tx, ty, tz, -1, -1,sy);
				}else if ((tx-sx<0)&&(tz-sz>0)) {
					return checkDiagonalBlocks(tx, ty, tz, 1, -1,sy);					
				}else if ((tx-sx<0)&&(tz-sz<0)) {
					return checkDiagonalBlocks(tx, ty, tz, 1, 1,sy);					
				}else {
					return checkDiagonalBlocks(tx, ty, tz, -1, 1,sy);					
				}
			} else {
				if ((tx-sx>0)) {
					return checkStraightBlocks(tx, ty, tz, -1, 0,sy);
				}else if ((tx-sx<0)) {
					return checkStraightBlocks(tx, ty, tz, 1, 0,sy);					
				}else if ((tz-sz<0)) {
					return checkStraightBlocks(tx, ty, tz, 0, 1,sy);					
				}else {
					return checkStraightBlocks(tx, ty, tz, 0, -1,sy);					
				}
			}
		} else {
			return false;
		}
	}
	
	private boolean checkStraightBlocks(int tx, int ty, int tz, int rx, int rz, int sy) {
		int obstacle;
		if (ty-sy>0) {
			try {
				obstacle = world.getBlock(tx+rx, ty+1, tz+rz).getId();
				//ConsoleIO.println("tx="+(tx+rx)+" ty="+(ty+1)+" tz="+(tz+rz)+" obstacleID:"+obstacle);
			} catch (NullPointerException e) {
				obstacle = 0;
			}
		} else if (ty-sy<0) {
			try {
				obstacle = world.getBlock(tx, ty+2, tz).getId();
			} catch (NullPointerException e) {
				obstacle = 0;
			}
		} else {
			obstacle = 0;
		}
		
		return (obstacle==0);
	}

	private boolean checkDiagonalBlocks(int tx, int ty, int tz, int rx, int rz, int sy) {
		int obstacle1;
		int obstacle2;
		int obstacle3;
		int obstacle4;
		int ry = 0;
		
		if (ty-sy<0) {
			ry = 1;
		}
		try {
			obstacle1 = world.getBlock(tx+rx, ty+ry, tz).getId();
		} catch (NullPointerException e) {
			obstacle1 = 0;
		}
		try {
			obstacle3 = world.getBlock(tx+rx, ty+ry+1, tz).getId();
		}catch (NullPointerException e) {
			obstacle3=0;
		}
		try {
			obstacle2 = world.getBlock(tx, ty+ry, tz+rz).getId();
		} catch (NullPointerException e) {
			obstacle2 = 0;
		}
		try {
			obstacle4 = world.getBlock(tx, ty+ry+1, tz+rz).getId();
		} catch (NullPointerException e) {
			obstacle4 = 0;
		}
		
		/*	Add block ids here that you want (==) the client to be able to walk through.
		 */
		
		if ((ty-sy>0)||(ty-sy<0)) {
			if (obstacle3==0&&obstacle4==0&&(obstacle1==0||obstacle1==31||obstacle1==38||obstacle1==171)&&(obstacle2==0||obstacle2==31||obstacle2==38||obstacle2==171||obstacle2!=18)) {
				return true;
			} else {
				return false;
			}
		} else if (((obstacle1==0||obstacle1==31||obstacle1==38||obstacle1==171)&&obstacle3==0)||((obstacle2==0||obstacle2==31||obstacle2==38||obstacle2==171)&&obstacle4==0)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean inOpenList(Surface node) {
		for (Surface o : open) {
			if (o.equals(node))
				return true;
		}
		return false;
	}

	private boolean inClosedList(Surface node) {
		for (Surface o : closed) {
			if (o.equals(node))
				return true;
		}
		return false;
	}

	public boolean isDone() {
		return done;
	}

	public void setDoneToFalse() {
		done = false;
	}

	private void removeOpenList(Surface node) {
		open.remove(node);
	}

	private void removeClosedList(Surface node) {
		closed.remove(node);
	}







}