package org.koekepan.herobrine.client.pathfinding;

import java.util.ArrayList;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.client.pathfinding.Graph;

public class Path{
	private ArrayList<Step> steps = new ArrayList<Step>();

	private double previousX;
	private double previousY;
	private double previousZ;

	private Graph graph;

	public Path(double x, double y, double z, Graph graph) {
		previousX = Math.floor(x);
		previousY = Math.floor(y);
		previousZ = Math.floor(z);

		this.graph = graph;
	}

	public int getLength() {
		return steps.size();
	}

	public boolean getType(int index) {
		return steps.get(index).getJump();
	}
	public boolean getJumpDirection(int index) {
		return steps.get(index).getUp();
	}

	public double getX(int index){
		return steps.get(index).x;
	}

	public double getY(int index){
		return steps.get(index).y;
	}

	public double getZ(int index){
		return steps.get(index).z;
	}

	public void addStep(int x, int y, int z, boolean jump, boolean up) {
		Step step = new Step(x,y,z, jump, up);
		steps.add(step);
	}

	public String prependStep(double x, double y, double z, int pathLength) {
		//avoiding diagonal objects
		int tempx = (int)x;
		int tempz = (int)z;
		double x2;
		double z2;
		boolean jump = false;
		boolean up = true;
		//ConsoleIO.println("Target x="+x+"Target y="+y+" Target z="+z);
		//ConsoleIO.println("PrevX="+previousX+" PrevY:"+previousY+" PrevZ="+previousZ);
		if (y-previousY==0) {
			if ((x-previousX!=0) && (z-previousZ!=0)) {
				if ((x-previousX >0)&&(z-previousZ>0)) {
					Surface obstacle1 = graph.getSurface(tempx-1, tempz);
					Surface obstacle2 = graph.getSurface(tempx, tempz-1);

					if (obstacle1 == null) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.1;
						x2 = x+0.6;
						z2 = z+0.3; 
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));
					}	else if (obstacle2 == null) {
						previousX= previousX+0.1;
						previousZ = previousZ+0.9;
						x2 = x+0.3;
						z2 = z+0.6;	 
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));					
					}
					/*if (obstacle1 == null) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.1;
						x2 = x+0.6;
						z2 = z+0.3; 
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));
					} else if(obstacle1.getId()==0) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.1;
						x2 = x+0.6;
						z2 = z+0.3; 
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));						
					} else if (obstacle2 == null) {
						previousX= previousX+0.1;
						previousZ = previousZ+0.9;
						x2 = x+0.3;
						z2 = z+0.6;	 
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));					
					} else if(obstacle2.getId()==0) {
						previousX= previousX+0.1;
						previousZ = previousZ+0.9;
						x2 = x+0.3;
						z2 = z+0.6;	 
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));						
					}*/
				} else if ((x-previousX <0)&&(z-previousZ>0)) {
					Surface obstacle1 = graph.getSurface(tempx+1, tempz);
					Surface obstacle2 = graph.getSurface(tempx, tempz-1);
					
					if (obstacle1 == null) {
						previousX = previousX+0.1;
						previousZ = previousZ+0.1;
						x2 = x+0.3;
						z2 = z+0.3;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false,up));
					} else if (obstacle2 == null) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.9;
						x2 = x+0.6;
						z2 = z+0.6;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));				
					}
					/*if (obstacle1 == null) {
						previousX = previousX+0.1;
						previousZ = previousZ+0.1;
						x2 = x+0.3;
						z2 = z+0.3;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false,up));
					} else if(obstacle1.getId()==0) {
						previousX = previousX+0.1;
						previousZ = previousZ+0.1;
						x2 = x+0.3;
						z2 = z+0.3;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false,up));						
					} else if (obstacle2 == null) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.9;
						x2 = x+0.6;
						z2 = z+0.6;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));				
					} else if(obstacle2.getId()==0) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.9;
						x2 = x+0.6;
						z2 = z+0.6;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));						
					}*/
				} else if ((x-previousX <0)&&(z-previousZ<0)) {
					Surface obstacle1 = graph.getSurface(tempx, tempz+1);
					Surface obstacle2 = graph.getSurface(tempx+1, tempz);
					
					if (obstacle1 == null) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.1;
						x2 = x+0.6;
						z2 = z+0.3;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));
					} else if (obstacle2 == null) {
						previousX = previousX+0.1;
						previousZ = previousZ+0.9;
						x2 = x+0.3;
						z2 = z+0.6;	 
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));					
					}
					/*if (obstacle1 == null) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.1;
						x2 = x+0.6;
						z2 = z+0.3;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));
					} else if(obstacle1.getId()==0) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.1;
						x2 = x+0.6;
						z2 = z+0.3;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));						
					} else if (obstacle2 == null) {
						previousX = previousX+0.1;
						previousZ = previousZ+0.9;
						x2 = x+0.3;
						z2 = z+0.6;	 
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));					
					} else if(obstacle2.getId()==0) {
						previousX = previousX+0.1;
						previousZ = previousZ+0.9;
						x2 = x+0.3;
						z2 = z+0.6;	 
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));							
					}*/
				} else if ((x-previousX >0)&&(z-previousZ<0)) {
					Surface obstacle1 = graph.getSurface(tempx, tempz+1);
					Surface obstacle2 = graph.getSurface(tempx-1, tempz);
					
					if (obstacle1 == null) {
						previousX = previousX+0.1;
						previousZ = previousZ+0.1;
						x2 = x+0.3;
						z2 = z+0.3;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));
					} else if (obstacle2 == null) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.9;
						x2 = x+0.6;
						z2 = z+0.6;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));					
					}
					/*if (obstacle1 == null) {
						previousX = previousX+0.1;
						previousZ = previousZ+0.1;
						x2 = x+0.3;
						z2 = z+0.3;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));
					} else if(obstacle1.getId()==0) {
						previousX = previousX+0.1;
						previousZ = previousZ+0.1;
						x2 = x+0.3;
						z2 = z+0.3;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));						
					} else if (obstacle2 == null) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.9;
						x2 = x+0.6;
						z2 = z+0.6;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));					
					} else if(obstacle2.getId()==0) {
						previousX = previousX+0.9;
						previousZ = previousZ+0.9;
						x2 = x+0.6;
						z2 = z+0.6;
						steps.add(0, new Step(previousX,previousY,previousZ, false, up));
						steps.add(0, new Step(x2, y, z2, false, up));								
					}*/
				}
			}

		} else {
			jump = true;
			if (previousY-y<0) {
				up = false;
			}
			steps.get(0).updateStep(jump, up);			
		}

		previousX = x;
		previousY = y;
		previousZ = z;
		//make sure that the avatar moves to the middle of the square
		x = x+0.5;
		z = z+0.5;
		Step step = new Step(x, y, z, jump, up);
		steps.add(0, step);
		return step.toString();

	}

	public Step getStep(int index) {
		return steps.get(index);
	}

	public String toString() {
		String output = "";
		for (Step step : steps) {
			output = output + step.toString()+" ";
		}
		return output;
	}

	public class Step{
		private double x;
		private double y;
		private double z;

		private boolean jump;
		private boolean up;

		public Step(double x, double y, double z, boolean jump, boolean up) {
			this.x = x;
			this.y = y;
			this.z = z;

			this.jump = jump;
			this.up = up;
			ConsoleIO.println(toString());
		}

		public double getX() {
			return x;
		}

		public double getY() {
			return y;
		}

		public double getZ() {
			return z;
		}

		public boolean getJump() {
			return jump;
		}

		public boolean getUp() {
			return up;
		}
		private void updateStep(boolean jump, boolean up) {
			this.jump = jump;
			this.up = up;
		}

		public String toString() {
			return "["+x+","+y+","+z+"] Jump="+jump+" Up:"+up;
		}
	}
}