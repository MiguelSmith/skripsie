package org.koekepan.herobrine.client;

import org.koekepan.herobrine.client.packet.PacketListener;
import org.spacehq.packetlib.packet.Packet;
import org.koekepan.herobrine.client.state.ClientState;

public interface ClientEmulator
{
	public String getName();
	public String getHost();
	public int getPort();
	
	public void setHost(String host);
	public void setPort(int port);

	public void addPacketListener(PacketListener packetListener);
	public void send(Packet packet);
	
	public void initializeSession();
	public boolean isConnected();
	public void connect();
	public void disconnect();
	
	public void addWaypoint(int x, int y, int z);
	
	public ClientControl getClientControl();
	public ClientState getClientState();
}
