package org.koekepan.herobrine.client;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.client.packet.PacketAdapter;
import org.koekepan.herobrine.client.packet.PacketHandler;
import org.koekepan.herobrine.client.packet.PacketListener;
import org.koekepan.herobrine.client.packet.PacketSession;
import org.koekepan.herobrine.client.packet.behaviours.PacketBehaviours;
import org.koekepan.herobrine.client.state.ClientState;
import org.spacehq.mc.protocol.MinecraftProtocol;
import org.spacehq.packetlib.Client;
import org.spacehq.packetlib.packet.Packet;
import org.spacehq.packetlib.tcp.TcpSessionFactory;

public class HerobrineClient implements ClientEmulator {

	private String host = "localhost";
	private int port = 25565;
	private String name = "Herobrine";
	private int interval = 50;
	private int waitTime = 100;
	private ClientControl control;

	private ClientState state;

	private MinecraftProtocol protocol;
	private Client client;
	private PacketSession session;
	private PacketHandler packetHandler;
	private ClientLoop loop;
	
	Future<?> connectTask;
	ExecutorService connectExecutor;
	
	List<PacketListener> packetListeners = new ArrayList<PacketListener>();

	public HerobrineClient(String name, int interval, ClientControl control) {

		this.name = name; 
		this.interval = interval;
		this.control = control;

		state = new ClientState();
	}
	
	public HerobrineClient(String name, int interval, ClientControl control, String host, int port) {
		this(name, interval, control);
		this.host = host;
		this.port = port;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getHost() {
		return host;
	}

	@Override
	public int getPort() {
		return port;
	}

	@Override
	public void setHost(String host) {
		if(!isConnected()) {
			this.host = host;
		}
	}

	@Override
	public void setPort(int port) {
		if(!isConnected()) {
			this.port = port;
		}
	}
	
	public void addWaypoint(int x, int y, int z) {
		control.addWaypoint(x, y, z);
	}


	@Override
	public void addPacketListener(PacketListener packetListener) {
		packetListeners.add(packetListener);
		client.getSession().addListener(new PacketAdapter(packetListener));
	}

	@Override
	public void send(Packet packet) {
		//if(isConnected()) {
			session.send(packet);
		//}
	}

	@Override
	public boolean isConnected() {
		boolean connected = false;
		if(client != null && client.getSession() != null) {
			connected = client.getSession().isConnected();
		} 
		return connected;
	}

	@Override
	public void connect() {	
		if (client == null) this.initializeSession();
		
		connectExecutor = Executors.newSingleThreadExecutor();
		connectTask = connectExecutor.submit(new Runnable() {
			@Override
			public void run()  {
				client.getSession().connect(true);
				
				while(!state.getPlayerState().getSpawned()) {
					try {
						Thread.sleep(waitTime);
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
					}
				}
			}
		});
		
		try {
			connectTask.get(5, TimeUnit.SECONDS);
			loop.start();
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			ConsoleIO.println("ERROR: Player \"" + name + "\" failed to connect to <" + client.getHost() + ":" + client.getPort() + ">"); // TODO OUTPUT: ERROR: Player "NAME" failed to connect to <HOST:PORT>
			
			loop.stop();
			client.getSession().disconnect("Disconnect", true);
			connectTask.cancel(true);
			connectExecutor.shutdown();
		}

	}

	@Override
	public void disconnect() {
		loop.stop();
		if (client.getSession() != null) {
			client.getSession().disconnect("Finished.", true);
		}
	}


	public void initializeSession() {
		state.getPlayerState().setSpawned(false);
		
		protocol = new MinecraftProtocol(name);
		client = new Client(host, port, protocol, new TcpSessionFactory(null));
		session = new PacketSession(client);
		packetHandler = new PacketHandler(new PacketBehaviours(session, state));
		client.getSession().addListener(new PacketAdapter(packetHandler));
		
		for (PacketListener packetListener : packetListeners) {
			ConsoleIO.println("Player \"" + name + "\": Adding PacketListener ("+packetListener.getClass().getSimpleName() + ")"); // TODO OUTPUT: Player "NAME": Adding PacketListener (LISTENER CLASS)
			client.getSession().addListener(new PacketAdapter(packetListener));			
		}

		// set up client control
		control.setup(name, interval, session, state);
		loop = new ClientLoop();
	}

	
	@SuppressWarnings("unused")
	private void terminateSession() {
		protocol = null;
		client = null;
		session = null;
		packetHandler = null;
		loop = null;
	}


	private class ClientLoop {

		private Future<?> clientFuture;
		private ScheduledExecutorService clientExecutor = Executors.newSingleThreadScheduledExecutor();
		private Future<?> stateFuture;	
		//private Future<?> worldFuture;
		
		//private ScheduledExecutorService worldExecutor = Executors.newSingleThreadScheduledExecutor();
		private ExecutorService stateExecutor = Executors.newSingleThreadExecutor();
		
		public ClientLoop() {
			stateFuture = stateExecutor.submit(packetHandler);	
			//worldFuture = worldExecutor.submit(state.getWorldState());
		}

		public void start() {	
			ConsoleIO.println("Reaches here (HerobrineClient Start)");
			clientFuture = clientExecutor.scheduleAtFixedRate(new Runnable(){

				@Override
				public void run() {
					try {
						stateFuture.cancel(true);
						while(!stateFuture.isDone()) {
							Thread.sleep(1);
						}
						
						/*worldFuture.cancel(true);
						while(!worldFuture.isDone()) {
							Thread.sleep(1);
						}*/

						control.run();

						stateFuture = stateExecutor.submit(packetHandler);
						//worldFuture = worldExecutor.submit(state.getWorldState());

					} catch (InterruptedException e) {
						ConsoleIO.println("Got interrupted");
						Thread.currentThread().interrupt();
					}
				}			
			}, 0, interval, TimeUnit.MILLISECONDS);
		}

		public void stop() {
			if (stateFuture != null) {

				stateFuture.cancel(true);
				stateExecutor.shutdown();
				while(!stateFuture.isDone()) {
					try { while(!stateFuture.isDone()) {
							Thread.sleep(1);
						}
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
					}
					
				}

			}
			
			if (clientFuture != null) {
				clientFuture.cancel(true);
				clientExecutor.shutdown();
				while(!clientFuture.isDone()) {
					try { while(!clientFuture.isDone()) {
							Thread.sleep(1);
						}
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
					}
					
				}
			}
		}

	}
	
	public ClientControl getClientControl() {
		return this.control;
	}
	
	
	public void setClientControl(ClientControl control) {
		this.control = control;
	}
	
	public ClientState getClientState() {
		return this.state;
	}

}
