package org.koekepan.herobrine.client;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.client.packet.PacketListener;
import org.koekepan.herobrine.client.packet.PacketSession;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.player.PlayerState;
import org.spacehq.mc.protocol.packet.ingame.client.player.ClientPlayerPositionRotationPacket;
import org.spacehq.packetlib.packet.Packet;

@SuppressWarnings("unused")
public class HerobrineControl implements ClientControl {
	
	private final static double SPEED = 4.317; 
	
	private String name;
	private int interval;
	private ClientState state;
	private PacketSession session;

	private byte waypoint = 1; // next waypoint is 1

	private int x;
	private int z;
	private int radius;
	
	private int xTarget;
	private int zTarget;

	public HerobrineControl(int x , int z, int radius) {
		this.x = x;
		this.z = z;
		this.radius = radius;
		
		// start moving towards waypoint 0
		xTarget = x + radius;
		zTarget = z + radius;
	}

	@Override
	public void setup(String name ,int interval, PacketSession session, ClientState state) {
		this.name = name;
		this.interval = interval;
		this.session = session;
		this.state = state;
	}
	
	@Override
	public PacketListener getPacketListener() {
		return null;
	}	
	
	public void addWaypoint(int x, int y, int z) {
		//Useless implementation
	}

	@Override
	public void run() {	
		ConsoleIO.println("HerobrineControl run");

		PlayerState player = state.getPlayerState();
		
		double dx = xTarget - player.getX();
		double dz = zTarget - player.getZ();
		double ds = Math.sqrt(dx*dx + dz*dz);//TODO change to heuristic
		
		if(ds < 0.2) {
			switch(waypoint) {
			case 0: // x+ z+
				xTarget = x + radius;
				zTarget = z + radius;
				waypoint = 1;
				break;
			case 1: // x+ z-
				xTarget = x + radius;
				zTarget = z - radius;
				waypoint = 2;
				break;
			case 2: // x- z-
				xTarget = x - radius;
				zTarget = z - radius;
				waypoint = 3;
				break;
			case 3: // x- z+
				xTarget = x - radius;
				zTarget = z + radius;
				waypoint = 0;
				break;
			default:
				waypoint = 0;
			}
			
			dx = xTarget - player.getX();
			dz = zTarget - player.getZ();
			ds = Math.sqrt(dx*dx + dz*dz);
		}
		
		double k = (interval/1000.0) * (SPEED/ds);
		double x = player.getX() + dx*k;
		double z = player.getZ() + dz*k;
		float yaw = (float) Math.toDegrees(Math.atan2(-dx, dz));
		
		Packet out = new ClientPlayerPositionRotationPacket(true, x, player.getY(), z, yaw, 0);
		session.send(out);
	}

}