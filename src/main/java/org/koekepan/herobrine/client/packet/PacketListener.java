package org.koekepan.herobrine.client.packet;

import org.spacehq.packetlib.packet.Packet;

public interface PacketListener {
	public void packetReceived(Packet packet);
	public void packetSent(Packet packet);
}
