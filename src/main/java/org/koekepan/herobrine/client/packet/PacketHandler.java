package org.koekepan.herobrine.client.packet;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.behaviour.BehaviourHandler;
import org.spacehq.mc.protocol.packet.login.server.LoginDisconnectPacket;
import org.spacehq.packetlib.packet.Packet;

public class PacketHandler implements Runnable, PacketListener {

	private BlockingQueue<Packet> packets = new LinkedBlockingQueue<Packet>();

	private BehaviourHandler<Packet> behaviours;

	public PacketHandler(BehaviourHandler<Packet> behaviours) {
		this.behaviours = behaviours;
	}

	public BlockingQueue<Packet> getPackets() {
		return packets;
	}

	public BehaviourHandler<Packet> getBehaviours() {
		return behaviours;
	}

	public void process(Packet packet) {
		behaviours.process(packet);
	}

	private void addPacketToQueue(Packet packet) {
		try {
			if(behaviours.hasBehaviour(packet.getClass())) {
				packets.put(packet);
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	@Override
	public void packetReceived(Packet packet) {
		//ConsoleIO.println("PacketHandler::packetReceived => Received a packet <"+packet.getClass()+">");
		if (packet instanceof LoginDisconnectPacket) {
			LoginDisconnectPacket disconnectPacket = (LoginDisconnectPacket)packet;			
			ConsoleIO.println("PacketHandler::packetReceived => Login unsuccessful with reason: " + disconnectPacket.getReason()); // TODO OUTPUT: PacketHandler::packetReceived => Login unsuccessful with reason: REASON
		}
		
		addPacketToQueue(packet);
	}

	@Override
	public void packetSent(Packet packet) {
		//ConsoleIO.println("PacketHandler::packetSent => Sent a packet <"+packet.getClass()+">");
		addPacketToQueue(packet);
	}

	@Override
	public void run() {
		try {
			while(!Thread.currentThread().isInterrupted()) {
				behaviours.process(packets.take());
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
