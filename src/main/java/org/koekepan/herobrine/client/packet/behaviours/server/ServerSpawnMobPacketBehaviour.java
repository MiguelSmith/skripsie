package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.packet.ingame.server.entity.spawn.ServerSpawnMobPacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.entity.EntityState;
import org.koekepan.herobrine.client.state.entity.MobEntity;

public class ServerSpawnMobPacketBehaviour
implements Behaviour<Packet>
{
	private EntityState entities;

	
	@SuppressWarnings("unused")
	private ServerSpawnMobPacketBehaviour() {}
	
	public ServerSpawnMobPacketBehaviour(ClientState state) {
		entities = state.getEntityState();
	}
	
	
	@Override
	public void process(Packet object)
	{
		ServerSpawnMobPacket in  = (ServerSpawnMobPacket) object;
			
		entities.addEntity(in.getEntityId(), new MobEntity(in));
	}

}
