package org.koekepan.herobrine.client.packet.behaviours.server;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.entity.Entity;
import org.koekepan.herobrine.client.state.entity.EntityState;
import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerEntityRotationPacket;
import org.spacehq.packetlib.packet.Packet;

public class ServerEntityRotationPacketBehaviour
implements Behaviour<Packet>
{
	private EntityState entities;
	
	
	@SuppressWarnings("unused")
	private ServerEntityRotationPacketBehaviour() {}
	
	public ServerEntityRotationPacketBehaviour(ClientState state) {
		entities = state.getEntityState();
	}
	
	
	@Override
	public void process(Packet object)
	{
		ServerEntityRotationPacket in = (ServerEntityRotationPacket) object;
		
		synchronized(entities) {
			Entity entity = entities.getEntity(in.getEntityId());
			entity.setYaw(in.getYaw());
			entity.setPitch(in.getPitch());
			entity.setOnGround(in.isOnGround());	
		}
	}

}
