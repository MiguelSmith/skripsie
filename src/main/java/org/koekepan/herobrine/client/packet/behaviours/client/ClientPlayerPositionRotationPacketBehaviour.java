package org.koekepan.herobrine.client.packet.behaviours.client;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.player.PlayerState;
import org.spacehq.mc.protocol.packet.ingame.client.player.ClientPlayerPositionRotationPacket;
import org.spacehq.packetlib.packet.Packet;

public class ClientPlayerPositionRotationPacketBehaviour implements Behaviour<Packet>{

	private PlayerState player;
	
	@SuppressWarnings("unused")
	private ClientPlayerPositionRotationPacketBehaviour() {}
	
	public ClientPlayerPositionRotationPacketBehaviour(ClientState state) {
		this.player = state.getPlayerState();
	}
	
	@Override
	public void process(Packet object) {
		ClientPlayerPositionRotationPacket in = (ClientPlayerPositionRotationPacket) object;				
		player.setX(in.getX());
		player.setY(in.getY());
		player.setZ(in.getZ());
		player.setPitch((float) in.getPitch());
		player.setYaw((float) in.getYaw());
		player.setOnGround(in.isOnGround());
	}

}
