package org.koekepan.herobrine.client.packet;

import org.spacehq.packetlib.Client;
import org.spacehq.packetlib.packet.Packet;

public class PacketSession {
	
	public Client client;
	
	public PacketSession(Client client) {
		this.client = client;
	}
	
	public void send(Packet packet) {
		client.getSession().send(packet);
	}
}
