package org.koekepan.herobrine.client.packet.behaviours;

import org.koekepan.herobrine.behaviour.BehaviourHandler;
import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.packet.PacketSession;
import org.koekepan.herobrine.client.packet.behaviours.*;
import org.koekepan.herobrine.client.packet.behaviours.client.ClientPlayerPositionRotationPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerBlockChangePacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerChunkDataPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerDestroyEntitiesPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerEntityPositionPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerEntityPositionRotationPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerEntityRotationPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerEntityTeleportPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerExplosionPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerJoinGamePacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerMultiBlockChangePacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerMultiChunkDataPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerPlayerAbilitiesPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerPlayerPositionRotationPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerSpawnMobPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerSpawnPlayerPacketBehaviour;
import org.koekepan.herobrine.client.packet.behaviours.server.ServerUpdateTimePacketBehaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.command.behaviours.movement.JumpCommandBehaviour;
import org.spacehq.mc.protocol.packet.ingame.client.player.ClientPlayerPositionRotationPacket;
import org.spacehq.mc.protocol.packet.ingame.server.*;
import org.spacehq.mc.protocol.packet.ingame.server.entity.*;
import org.spacehq.mc.protocol.packet.ingame.server.entity.player.*;
import org.spacehq.mc.protocol.packet.ingame.server.entity.spawn.*;
import org.spacehq.mc.protocol.packet.ingame.server.scoreboard.*;
import org.spacehq.mc.protocol.packet.ingame.server.window.*;
import org.spacehq.mc.protocol.packet.ingame.server.world.*;
import org.spacehq.mc.protocol.packet.login.server.*;
import org.spacehq.mc.protocol.packet.status.server.*;
import org.spacehq.packetlib.Client;
import org.spacehq.packetlib.packet.Packet;

@SuppressWarnings("unused")
public class PacketBehaviours extends BehaviourHandler<Packet> {
	
	private PacketSession session;
	private ClientState state;
	
	public PacketBehaviours(PacketSession session, ClientState state) {
		this.session = session;
		this.state = state;
		registerDefaultBehaviours();
	}
	
	public void registerDefaultBehaviours() {
		
		clearBehaviours();
		
		// server packets
		
		// entity
		registerBehaviour(ServerSpawnPlayerPacket.class, new ServerSpawnPlayerPacketBehaviour(state));									// 0x0C Spawn Player
		registerBehaviour(ServerSpawnMobPacket.class, new ServerSpawnMobPacketBehaviour(state));										// 0x0F Spawn Mob
		registerBehaviour(ServerDestroyEntitiesPacket.class, new ServerDestroyEntitiesPacketBehaviour(state));							// 0x13 Destroy Entities
		registerBehaviour(ServerEntityPositionPacket.class, new ServerEntityPositionPacketBehaviour(state));							// 0x15 Entity Relative Move
		registerBehaviour(ServerEntityRotationPacket.class, new ServerEntityRotationPacketBehaviour(state));							// 0x16 Entity Look
		registerBehaviour(ServerEntityPositionRotationPacket.class, new ServerEntityPositionRotationPacketBehaviour(state));			// 0x17 Entity Look And Relative Move
		registerBehaviour(ServerEntityTeleportPacket.class, new ServerEntityTeleportPacketBehaviour(state));							// 0x18 Entity Teleport
			
		// player
		registerBehaviour(ServerJoinGamePacket.class, new ServerJoinGamePacketBehaviour(state));										// 0x01 Join Game
		registerBehaviour(ServerPlayerPositionRotationPacket.class, new ServerPlayerPositionRotationPacketBehaviour(session, state));	// 0x08 Player Position And Look
		registerBehaviour(ServerPlayerAbilitiesPacket.class, new ServerPlayerAbilitiesPacketBehaviour(state));							// 0x39 Player Abilities
		
		// world
		registerBehaviour(ServerUpdateTimePacket.class, new ServerUpdateTimePacketBehaviour(state));									// 0x03 Time Update
		registerBehaviour(ServerChunkDataPacket.class, new ServerChunkDataPacketBehaviour(state));										// 0x21 Chunk Data
		registerBehaviour(ServerMultiBlockChangePacket.class, new ServerMultiBlockChangePacketBehaviour(state));						// 0x22 Multi Block Change
		registerBehaviour(ServerBlockChangePacket.class, new ServerBlockChangePacketBehaviour(state));									// 0x23 Block Change
		registerBehaviour(ServerMultiChunkDataPacket.class, new ServerMultiChunkDataPacketBehaviour(state));							// 0x26 Map Chunk Bulk
		registerBehaviour(ServerExplosionPacket.class, new ServerExplosionPacketBehaviour(state));										// 0x27 Explosion
		
		
		// client packet
		
		registerBehaviour(ClientPlayerPositionRotationPacket.class, new ClientPlayerPositionRotationPacketBehaviour(state));			// 0x06 Player Position And Look 
	}
	
}
