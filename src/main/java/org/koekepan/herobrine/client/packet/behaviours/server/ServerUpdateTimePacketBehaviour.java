package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.packet.ingame.server.world.ServerUpdateTimePacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.world.TimeUpdate;
import org.koekepan.herobrine.client.state.world.WorldState;

public class ServerUpdateTimePacketBehaviour
implements Behaviour<Packet>
{
	private WorldState world;
	
	
	@SuppressWarnings("unused")
	private ServerUpdateTimePacketBehaviour() {}
	
	public ServerUpdateTimePacketBehaviour(ClientState state) {
		world = state.getWorldState();
	}

	
	@Override
	public void process(Packet object) 
	{
		ServerUpdateTimePacket in = (ServerUpdateTimePacket) object;
		
		world.setTimeUpdate(new TimeUpdate(in));
	}

}
