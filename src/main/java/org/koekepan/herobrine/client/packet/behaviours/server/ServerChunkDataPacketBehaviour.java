package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.packet.ingame.server.world.ServerChunkDataPacket;
import org.spacehq.mc.protocol.util.ParsedChunkData;
import org.spacehq.packetlib.packet.Packet;
import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.world.WorldState;

public class ServerChunkDataPacketBehaviour
implements Behaviour<Packet>
{
	private WorldState world;
	
	
	@SuppressWarnings("unused")
	private ServerChunkDataPacketBehaviour() {}
	
	public ServerChunkDataPacketBehaviour(ClientState state) {
		this.world = state.getWorldState();
	}

	
	@Override
	public void process(Packet object) 
	{
		ServerChunkDataPacket in = (ServerChunkDataPacket) object;
		
		if(in.getChunks()[0] == null) {
			world.removeColumn(in.getX(), in.getZ());
		} else {
			world.setColumn(in.getX(), in.getZ(), new ParsedChunkData(in.getChunks(), in.getBiomeData()));
		}
	}

}
