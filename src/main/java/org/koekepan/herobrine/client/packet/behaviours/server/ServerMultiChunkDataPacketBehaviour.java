package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.packet.ingame.server.world.ServerMultiChunkDataPacket;
import org.spacehq.mc.protocol.util.ParsedChunkData;
import org.spacehq.packetlib.packet.Packet;
import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.world.WorldState;

public class ServerMultiChunkDataPacketBehaviour
implements Behaviour<Packet>
{
	private WorldState world;
	
	
	@SuppressWarnings("unused")
	private ServerMultiChunkDataPacketBehaviour() {}
	
	public ServerMultiChunkDataPacketBehaviour(ClientState state) {
		this.world = state.getWorldState();
	}
	
	
	@Override
	public void process(Packet object) 
	{
		ServerMultiChunkDataPacket in = (ServerMultiChunkDataPacket) object;
			
		synchronized(world) {
			for(int i = 0; i < in.getColumns(); i++) {				
				if(in.getChunks(i)[0] == null) {
					world.removeColumn(in.getX(i), in.getZ(i));
				} else {
					world.setColumn(in.getX(i), in.getZ(i), new ParsedChunkData(in.getChunks(i), in.getBiomeData(i)));
					//world.add
					//ConsoleIO.println("Column number "+i+" loaded at "+in.getX(i)+" "+in.getZ(i));
				}
			}	
		}
	}

}
