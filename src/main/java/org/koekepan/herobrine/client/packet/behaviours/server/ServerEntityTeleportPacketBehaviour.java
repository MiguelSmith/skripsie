package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerEntityTeleportPacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.entity.Entity;
import org.koekepan.herobrine.client.state.entity.EntityState;

public class ServerEntityTeleportPacketBehaviour
implements Behaviour<Packet>
{
	private EntityState entities;
	
	
	@SuppressWarnings("unused")
	private ServerEntityTeleportPacketBehaviour() {}
	
	public ServerEntityTeleportPacketBehaviour(ClientState state) {
		entities = state.getEntityState();
	}
	

	@Override
	public void process(Packet object)
	{
		ServerEntityTeleportPacket in = (ServerEntityTeleportPacket) object;
			
		synchronized(entities) {
			Entity entity = entities.getEntity(in.getEntityId());
			entity.setX(in.getX());
			entity.setY(in.getY());
			entity.setZ(in.getZ());
			entity.setYaw(in.getYaw());
			entity.setPitch(in.getPitch());
			entity.setOnGround(in.isOnGround());	
		}
	}

}
