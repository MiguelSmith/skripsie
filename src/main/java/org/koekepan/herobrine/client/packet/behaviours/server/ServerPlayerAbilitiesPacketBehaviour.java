package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.packet.ingame.server.entity.player.ServerPlayerAbilitiesPacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.player.PlayerAbilities;
import org.koekepan.herobrine.client.state.player.PlayerState;

public class ServerPlayerAbilitiesPacketBehaviour
implements Behaviour<Packet>
{
	private PlayerState player;
	
	
	@SuppressWarnings("unused")
	private ServerPlayerAbilitiesPacketBehaviour() {}
	
	public ServerPlayerAbilitiesPacketBehaviour(ClientState state) {
		player = state.getPlayerState();
	}
	
	
	@Override
	public void process(Packet packet) {
		
		ServerPlayerAbilitiesPacket in = (ServerPlayerAbilitiesPacket) packet;
		player.setAbilities(new PlayerAbilities(in.getInvincible(),in.getCanFly(), in.getFlying(), in.getCreative(), in.getFlySpeed(), in.getWalkSpeed()));
	}

}
