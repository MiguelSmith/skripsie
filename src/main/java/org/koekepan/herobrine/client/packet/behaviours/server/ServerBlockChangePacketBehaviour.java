package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.packet.ingame.server.world.ServerBlockChangePacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.world.Block;
import org.koekepan.herobrine.client.state.world.WorldState;

public class ServerBlockChangePacketBehaviour
implements Behaviour<Packet>
{
	private WorldState world;
	
	
	@SuppressWarnings("unused")
	private ServerBlockChangePacketBehaviour() {}
	
	public ServerBlockChangePacketBehaviour(ClientState state) {
		this.world = state.getWorldState();
	}

	
	@Override
	public void process(Packet object) 
	{
		ServerBlockChangePacket in = (ServerBlockChangePacket) object;				
		world.setBlock(in.getRecord().getPosition(), new Block(in.getRecord().getId(), in.getRecord().getData()));
	}

}
