package org.koekepan.herobrine.client.packet.behaviours.server;

import java.util.List;

import org.spacehq.mc.protocol.data.game.values.world.block.ExplodedBlockRecord;
import org.spacehq.mc.protocol.packet.ingame.server.world.ServerExplosionPacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.world.Block;
import org.koekepan.herobrine.client.state.world.WorldState;

public class ServerExplosionPacketBehaviour
implements Behaviour<Packet>
{
	private WorldState world;
	
	
	@SuppressWarnings("unused")
	private ServerExplosionPacketBehaviour() {}
	
	public ServerExplosionPacketBehaviour(ClientState state) {
		this.world = state.getWorldState();
	}
	
	
	@Override
	public void process(Packet object)
	{
		ServerExplosionPacket in = (ServerExplosionPacket) object;
			
		// TODO implement explosion knockback effect on entities
		synchronized(world) {
			List<ExplodedBlockRecord> records = in.getExploded();
			for(ExplodedBlockRecord record : records) {
				world.setBlock(record.getX(), record.getY(), record.getZ(), new Block(0));
			}
		}
	}

}
