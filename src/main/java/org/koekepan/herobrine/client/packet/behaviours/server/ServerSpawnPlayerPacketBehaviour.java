package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.packet.ingame.server.entity.spawn.ServerSpawnPlayerPacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.entity.EntityState;
import org.koekepan.herobrine.client.state.entity.PlayerEntity;

public class ServerSpawnPlayerPacketBehaviour
implements Behaviour<Packet>
{
	private EntityState entities;

	
	@SuppressWarnings("unused")
	private ServerSpawnPlayerPacketBehaviour() {}
	
	public ServerSpawnPlayerPacketBehaviour(ClientState state) {
		entities = state.getEntityState();
	}
	
	
	@Override
	public void process(Packet object)
	{
		ServerSpawnPlayerPacket in = (ServerSpawnPlayerPacket) object;

		entities.addEntity(in.getEntityId(), new PlayerEntity(in));
	}

}
