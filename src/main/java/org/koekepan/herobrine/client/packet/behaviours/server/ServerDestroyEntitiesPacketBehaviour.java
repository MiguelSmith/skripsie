package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerDestroyEntitiesPacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.entity.EntityState;

public class ServerDestroyEntitiesPacketBehaviour
implements Behaviour<Packet>
{
	private EntityState entities;
	
	
	@SuppressWarnings("unused")
	private ServerDestroyEntitiesPacketBehaviour() {}
	
	public ServerDestroyEntitiesPacketBehaviour(ClientState state) {
		entities = state.getEntityState();
	}

	
	@Override
	public void process(Packet object)
	{
		ServerDestroyEntitiesPacket in = (ServerDestroyEntitiesPacket) object;
			
		synchronized(entities) {
			int[] entityIds = in.getEntityIds();
			for(int entityId : entityIds) {
				entities.removeEntity(entityId);
			}
		}
	}

}
