package org.koekepan.herobrine.client.packet;

import org.spacehq.packetlib.event.session.PacketReceivedEvent;
import org.spacehq.packetlib.event.session.PacketSentEvent;
import org.spacehq.packetlib.event.session.SessionAdapter;

public class PacketAdapter extends SessionAdapter {

	private PacketListener listener;
	
	public PacketAdapter(PacketListener listener) {
		this.listener = listener;
	}
	
	@Override
	public void packetReceived(PacketReceivedEvent event) {
		listener.packetReceived(event.getPacket());
	}
	
	@Override
	public void packetSent(PacketSentEvent event) {
		listener.packetSent(event.getPacket());
	}
	
}
