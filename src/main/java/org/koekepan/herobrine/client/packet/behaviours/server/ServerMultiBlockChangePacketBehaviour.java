package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.data.game.values.world.block.BlockChangeRecord;
import org.spacehq.mc.protocol.packet.ingame.server.world.ServerMultiBlockChangePacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.world.Block;
import org.koekepan.herobrine.client.state.world.WorldState;

public class ServerMultiBlockChangePacketBehaviour
implements Behaviour<Packet>
{
	private WorldState world;
	
	
	@SuppressWarnings("unused")
	private ServerMultiBlockChangePacketBehaviour() {}
	
	public ServerMultiBlockChangePacketBehaviour(ClientState state) {
		this.world = state.getWorldState();
	}

	
	@Override
	public void process(Packet object) 
	{
		ServerMultiBlockChangePacket in = (ServerMultiBlockChangePacket) object;
			
		synchronized(world) {	
			BlockChangeRecord[] records = in.getRecords();
			for(BlockChangeRecord record : records) {
				world.setBlock(record.getPosition(), new Block(record.getId(), record.getData()));
			}
		}
	}

}
