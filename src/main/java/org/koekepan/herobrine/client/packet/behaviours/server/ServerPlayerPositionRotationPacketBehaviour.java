package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.data.game.values.ClientRequest;
import org.spacehq.mc.protocol.data.game.values.setting.ChatVisibility;
import org.spacehq.mc.protocol.packet.ingame.client.ClientRequestPacket;
import org.spacehq.mc.protocol.packet.ingame.client.ClientSettingsPacket;
import org.spacehq.mc.protocol.packet.ingame.client.player.ClientPlayerPositionRotationPacket;
import org.spacehq.mc.protocol.packet.ingame.server.entity.player.ServerPlayerPositionRotationPacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.packet.PacketSession;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.player.PlayerState;

public class ServerPlayerPositionRotationPacketBehaviour implements Behaviour<Packet> {

	private PacketSession session;
	private PlayerState player;

	private boolean spawned = false;

	public ServerPlayerPositionRotationPacketBehaviour(PacketSession session, ClientState state) {
		this.session = session;
		this.player = state.getPlayerState();
	}

	@Override
	public void process(Packet packet) {

		ServerPlayerPositionRotationPacket in = (ServerPlayerPositionRotationPacket) packet;

		player.setX(in.getX());
		player.setY(in.getY());
		player.setZ(in.getZ());
		player.setYaw(in.getYaw());
		player.setPitch(in.getPitch());

		Packet out = null;

		out = new ClientPlayerPositionRotationPacket(true, in.getX(), in.getY(), in.getZ(), in.getYaw(), in.getPitch());
		session.send(out);

		if(!spawned) {
			spawned = true;
			player.setSpawned(true);

			out = new ClientRequestPacket(ClientRequest.STATS);
			session.send(out);

			out = new ClientSettingsPacket("en_GB ", 5, ChatVisibility.FULL, false);
			session.send(out);
		}
	}

}
