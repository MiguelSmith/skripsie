package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerEntityPositionPacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.entity.Entity;
import org.koekepan.herobrine.client.state.entity.EntityState;

public class ServerEntityPositionPacketBehaviour
implements Behaviour<Packet>
{
	private EntityState entities;
	

	@SuppressWarnings("unused")
	private ServerEntityPositionPacketBehaviour() {}
	
	public ServerEntityPositionPacketBehaviour(ClientState state) {
		entities = state.getEntityState();
	}
	

	@Override
	public void process(Packet object)
	{
		ServerEntityPositionPacket in = (ServerEntityPositionPacket) object;
			
		synchronized(entities) {
			Entity entity = entities.getEntity(in.getEntityId());
			entity.setX(entity.getX() + in.getMovementX());
			entity.setY(entity.getY() + in.getMovementY());
			entity.setZ(entity.getZ() + in.getMovementZ());
			entity.setOnGround(in.isOnGround());	
		}
	}

}
