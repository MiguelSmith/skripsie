package org.koekepan.herobrine.client.packet.behaviours.server;

import org.spacehq.mc.protocol.packet.ingame.server.ServerJoinGamePacket;
import org.spacehq.packetlib.packet.Packet;

import org.koekepan.herobrine.behaviour.Behaviour;
import org.koekepan.herobrine.client.state.ClientState;
import org.koekepan.herobrine.client.state.player.PlayerState;

public class ServerJoinGamePacketBehaviour
implements Behaviour<Packet>
{
	private PlayerState player;
	
	
	@SuppressWarnings("unused")
	private ServerJoinGamePacketBehaviour() {}
	
	public ServerJoinGamePacketBehaviour(ClientState state) {
		player = state.getPlayerState();
	}

	
	@Override
	public void process(Packet object)
	{
		ServerJoinGamePacket in = (ServerJoinGamePacket) object;
			
		player.setEntityId(in.getEntityId());	
	}

}
