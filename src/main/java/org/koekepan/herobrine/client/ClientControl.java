 package org.koekepan.herobrine.client;

import org.koekepan.herobrine.client.packet.PacketListener;
import org.koekepan.herobrine.client.packet.PacketSession;
import org.koekepan.herobrine.client.state.ClientState;

public interface ClientControl extends Runnable {								
	public void setup(String name, int interval, PacketSession session, ClientState state);
	public void addWaypoint(int x, int y, int z);
	public PacketListener getPacketListener();
}
