package org.koekepan.herobrine.client.state;

import org.koekepan.herobrine.client.state.entity.EntityState;
import org.koekepan.herobrine.client.state.player.PlayerState;
import org.koekepan.herobrine.client.state.world.WorldState;

public class ClientState{
	
	private final EntityState entities;
	private final PlayerState player;
	private final WorldState world;
	
	public ClientState() {
		entities = new EntityState();
		player = new PlayerState();
		world = new WorldState();
	}
	
	public EntityState getEntityState() {
		return entities;
	}
	
	public PlayerState getPlayerState() {
		return player;
	}

	public WorldState getWorldState() {
		return world;
	}
	
}
