package org.koekepan.herobrine.client.state.world;

import org.spacehq.mc.protocol.packet.ingame.server.world.ServerUpdateTimePacket;

public class TimeUpdate
{
	private final long worldAge;
	private final long worldTime;
	
	
	public TimeUpdate()
	{
		worldAge = -1;
		worldTime = -1;
	}
	
	public TimeUpdate(long worldAge, long worldTime)
	{
		this.worldAge = worldAge;
		this.worldTime = worldTime;
	}
	
	public TimeUpdate(ServerUpdateTimePacket packet) {
		this(packet.getWorldAge(), packet.getTime());
	}
	
	
	public long getWorldAge() {
		return worldAge;
	}
	
	public long getWorldTime() {
		return worldTime;
	}

}
