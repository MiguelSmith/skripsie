package org.koekepan.herobrine.client.state.world;

public class Block
{
	private final int id;
	private final int data;
	private final int blockId;
	
	
	@SuppressWarnings("unused")
	private Block() {
		this(0);
	}
	
	public Block(int id, int data)
	{
		this.id = id;
		this.data = data;
		blockId = (id << 4) + (data & 0x000f);
	}
	
	public Block(int blockId)
	{
		this.blockId = blockId;
		id = blockId >> 4;
		data = (blockId & 0x000f);
	}
	
	
	public int getId() {
		return id;
	}
	
	public int getData() {
		return data;
	}
	
	public int getBlockId() {
		return blockId;
	}
	
	
    @Override
    public boolean equals(Object o) 
    {
        if (this == o) {
        	return true;
        }
        	
        if (!(o instanceof Block)) {
        	return false;
        }
        
        Block that = (Block) o;
        return this.id == that.id && this.data == that.data && this.blockId == that.blockId;
    }

    @Override
    public int hashCode() {
        return blockId;
    }
	
}
