package org.koekepan.herobrine.client.state.entity;

import java.util.HashMap;
import java.util.Map;

public class EntityState
{
	Map<Integer,Entity> entities;
	
	
	public EntityState() {
		entities = new HashMap<Integer, Entity>();
	}
	
	
	public synchronized void addEntity(int entityId, Entity entity) {
		entities.put(entityId, entity);
	}
	
	public synchronized Entity getEntity(int entityId) {
		return entities.get(entityId);
	}
	
	public synchronized Entity[] getEntities() {
		return entities.values().toArray(new Entity[entities.size()]);
	}
	
	public synchronized void removeEntity(int entityId) {
		entities.remove(entityId);
	}
	
	public synchronized int countEntities() {
		return entities.size();
	}
	
}
