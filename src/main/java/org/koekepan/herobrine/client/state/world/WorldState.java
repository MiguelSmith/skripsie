package org.koekepan.herobrine.client.state.world;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.IntPredicate;
import java.util.stream.*;

import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.client.pathfinding.Graph;
import org.koekepan.herobrine.client.pathfinding.Surface;
import org.spacehq.mc.protocol.data.game.Chunk;
import org.spacehq.mc.protocol.data.game.Position;
import org.spacehq.mc.protocol.util.ParsedChunkData;

import io.netty.handler.codec.http.multipart.InterfaceHttpPostRequestDecoder;

public class WorldState
{
	private final Map<ColumnKey, ParsedChunkData> columns;
	private List<ColumnKey> keys = new ArrayList<ColumnKey>();
	private volatile TimeUpdate timeUpdate;
	private Graph graph;
	
	private int[] blockList= {0,6,8,9,10,11,18,31,32,37,38,39,40,50,51,75,76};


	public WorldState()
	{
		columns= new ConcurrentHashMap<ColumnKey,ParsedChunkData>();
		timeUpdate = new TimeUpdate();
		graph = new Graph();
	}


	// block state
	
	public synchronized void setColumn(int x, int z, ParsedChunkData column) {
		columns.put(new ColumnKey(x, z), column);
	}

	public synchronized void removeColumn(int x, int z) {
		columns.remove(new ColumnKey(x, z));
	}

	public synchronized List<ParsedChunkData> getColumns() {
		return new ArrayList<ParsedChunkData>(columns.values());
	}

	public synchronized ParsedChunkData getColumn(int x, int z) {
		return columns.get(new ColumnKey(x, z));
	}
	
	public Graph getGraph() {
		return graph;
	}

	public synchronized int countColumns() {
		return columns.size();
	}
	
	public synchronized boolean areColumnsLoaded() {
		if (columns.size()==441) {
			return true;
		} else {
			return false;
		}
	}

	public synchronized void setBlock(int x, int y, int z, Block block) {
		setBlock(new Position(x, y, z), block);
	}
	
	public synchronized void setBlock(Position pos, Block block)
	{
		Chunk chunk = columns.get(new ColumnKey(chunkPos(pos.getX()), chunkPos(pos.getZ()))).getChunks()[chunkPos(pos.getY())];
		
		if(chunk != null) {
			chunk.getBlocks().set(relPos(pos.getX()), relPos(pos.getY()), relPos(pos.getZ()), block.getBlockId());
		}
	}

	public synchronized Block getBlock(int x, int y, int z) {
		//ConsoleIO.println("y pos = "+y);
		return getBlock(new Position(x, y, z));
	}
	
	public synchronized Block getBlock(Position pos)
	{
		//ConsoleIO.println("Gets to getBlock + "+pos.getX()+" "+pos.getY()+" "+pos.getZ());
		Chunk chunk = columns.get(new ColumnKey(chunkPos(pos.getX()), chunkPos(pos.getZ()))).getChunks()[chunkPos(pos.getY())];
		if(chunk != null) {
			//ConsoleIO.println("Gets into chunk!=null");
			return new Block(chunk.getBlocks().get(relPos(pos.getX()), relPos(pos.getY()), relPos(pos.getZ())));
		} else {
			return null;
		}
	}


	// coordinate of chunk in world
	private int chunkPos(int p) {
		return (int) Math.floor(p/16.0);
	}

	// coordinate of block in chunk
	public int relPos(int p)
	{
		int relP = p%16;
		return (relP < 0) ? (relP += 16) : relP;
	}


	// time state

	public synchronized void setTimeUpdate(TimeUpdate timeUpdate) {
		this.timeUpdate = timeUpdate; 
	}

	public synchronized TimeUpdate getTimeUpdate() {
		return timeUpdate;
	}
	
	public Surface setSurfaceBlock(int x, int sy, int z, int pathID) {
		//ConsoleIO.println("Start setSurfaceBlock + x="+x+" and z="+z);
		Surface surface = null;
			surface = graph.getSurface(x, z);
			if (surface==null) {
				Block block = null;
				int y = 0;
				for (int i=sy;i>=0;i--) {
					block = getBlock(x, i, z);
					if (block!=null) {
						if (block.getId()!=0 && block.getId()!=171&&block.getId()!=31&&block.getId()!=38) {
							i = i+2;
						} else {
							y = i;
							break;
						}
					} 
				}
				//ConsoleIO.println("Found surface block at ["+x+","+y+","+z+"]");
				
				surface = new Surface(x, y, z, block.getId(),pathID);
				graph.setSurface(surface, pathID);
			} else {
				if (!surface.comparePathID(pathID)) {
					surface.reset();
					graph.setSurface(surface, pathID);
				}
			}

		return surface;
	}
	
	public void updateGraph(Surface surface, int pathID) {
		graph.setSurface(surface, pathID);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
