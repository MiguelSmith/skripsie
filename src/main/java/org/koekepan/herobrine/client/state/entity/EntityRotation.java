package org.koekepan.herobrine.client.state.entity;

public class EntityRotation
{
	private final double pitch;
	private final double yaw;
	
	
	public EntityRotation() {
		this(0, 0);
	}
	
	public EntityRotation(double pitch, double yaw)
	{
		this.pitch = pitch;
		this.yaw = yaw;
	}
	
	
	public double getPitch() {
		return pitch;
	}
	
	public double getYaw() {
		return yaw;
	}
	
}
