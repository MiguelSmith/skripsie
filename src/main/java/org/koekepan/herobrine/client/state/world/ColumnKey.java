package org.koekepan.herobrine.client.state.world;

import java.io.Serializable;

public class ColumnKey implements Serializable
{
    private final int x;
    private final int z;

    
    public ColumnKey(int x, int z)
    {
        this.x = x;
        this.z = z;
    }
    
    
    public int getX() {
    	return x;
    }
    
    public int getZ() {
    	return z;
    }
    
    
    @Override
    public boolean equals(Object o) 
    {
        if (this == o) {
        	return true;
        }
        	
        if (!(o instanceof ColumnKey)) {
        	return false;
        }
        
        ColumnKey key = (ColumnKey) o;
        return x == key.x && z == key.z;
    }

    @Override
    public int hashCode()
    {
    	// 2097169 is the next prime larger than x or z's max value
        return 2097169 * x + z;
    }
}