package org.koekepan.herobrine.client.state.entity;

public class EntityPosition
{
	private final double x;
	private final double y;
	private final double z;
	
	
	public EntityPosition() {
		this(0, 0, 0);
	}
	
	public EntityPosition(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
}
