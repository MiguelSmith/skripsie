package org.koekepan.herobrine.client.state.entity;

public class Entity
{
	private volatile double x;
	private volatile double y;
	private volatile double z;
	private volatile float yaw;
	private volatile float pitch;
	private volatile boolean onGround;
	
	
	public Entity() {
		this(0, 0, 0, 0, 0);
	}
	
	public Entity(double x, double y, double z, float yaw, float pitch)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
		this.onGround = true;
	}
	
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public void setZ(double z) {
		this.z = z;
	}
	
	public void setYaw(float yaw) {
		this.yaw = yaw;
	}
	
	public void setPitch(float pitch) {
		this.pitch = pitch;
	}
	
	public void setOnGround(boolean onGround) {
		this.onGround = onGround;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public float getYaw() {
		return yaw;
	}
	
	public float getPitch() {
		return pitch;
	}
	
	public boolean getOnGround() {
		return onGround;
	}
	
}
