package org.koekepan.herobrine.client.state.player;

import org.koekepan.herobrine.client.state.entity.Entity;

public class PlayerState
extends Entity
{
	private volatile boolean spawned;
	private volatile int entityId;
	private volatile PlayerAbilities abilities;
	
	
	public PlayerState() {
		this(-1, 0, 0, 0, 0, 0);
	}
	
	public PlayerState(int entityId, double x, double y, double z, float yaw, float pitch) 
	{
		super(x, y, z, yaw, pitch);
		
		this.entityId = entityId;
		this.abilities = new PlayerAbilities();
		
		spawned = false;
	}
	
	
	public void setSpawned(boolean spawned) {
		this.spawned = spawned;
	}
	
	public boolean getSpawned() {
		return spawned;
	}
	
	
	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public int getEntityId() {
		return entityId;
	}
	
	
	public void setAbilities(PlayerAbilities abilities) {
		this.abilities = abilities;
	}

	public PlayerAbilities getAbilities() {
		return abilities;
	}

}
