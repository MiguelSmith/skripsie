package org.koekepan.herobrine.client.state.player;

import org.spacehq.mc.protocol.packet.ingame.server.entity.player.ServerPlayerAbilitiesPacket;

public class PlayerAbilities
{
	private final boolean invincible;
	private final boolean canFly;
	private final boolean flying;
	private final boolean creative;
	private final float flySpeed;
	private final float walkSpeed;
	
	
	// default walk speed is 4.317 blocks per second
	public PlayerAbilities() {
		this(false, false, false, false, 0, 4.317f);
	}
	
	public PlayerAbilities(boolean invincible, boolean canFly, boolean flying, boolean creative, float flySpeed, float walkSpeed)
	{
		this.invincible = invincible;
		this.canFly = canFly;
		this.flying = flying;
		this.creative = creative;
		this.flySpeed = flySpeed;
		this.walkSpeed = walkSpeed;
	}
	
	public PlayerAbilities(ServerPlayerAbilitiesPacket packet) {
		this(packet.getInvincible(),packet.getCanFly(), packet.getFlying(), packet.getCreative(), packet.getFlySpeed(), packet.getWalkSpeed());
	}
	
	
	public boolean getInvincible() {
		return invincible;
	}
	
	public boolean getCanFly() {
		return canFly;
	}
	
	public boolean getFlying() {
		return flying;
	}
	
	public boolean getCreative() {
		return creative;
	}
	
	public float getFlySpeed() {
		return flySpeed;
	}
	
	public float getWalkSpeed() {
		return walkSpeed;
	}
	
}
