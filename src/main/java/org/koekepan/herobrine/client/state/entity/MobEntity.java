package org.koekepan.herobrine.client.state.entity;

import org.spacehq.mc.protocol.packet.ingame.server.entity.spawn.ServerSpawnMobPacket;

public class MobEntity
extends Entity
{
	public MobEntity(double x, double y, double z, float yaw, float pitch) {
		super(x, y, z, yaw, pitch);
	}
	
	public MobEntity(ServerSpawnMobPacket packet) {
		this(packet.getX(), packet.getY(), packet.getZ(), packet.getYaw(), packet.getPitch());
	}
	
}

