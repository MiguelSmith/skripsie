package org.koekepan.herobrine.client.state.entity;

import org.spacehq.mc.protocol.packet.ingame.server.entity.spawn.ServerSpawnPlayerPacket;

public class PlayerEntity
extends Entity
{
	public PlayerEntity(double x, double y, double z, float yaw, float pitch) {
		super(x, y, z, yaw, pitch);
	}
	
	public PlayerEntity(ServerSpawnPlayerPacket packet) {
		this(packet.getX(), packet.getY(), packet.getZ(), packet.getYaw(), packet.getPitch());
	}
	
}
