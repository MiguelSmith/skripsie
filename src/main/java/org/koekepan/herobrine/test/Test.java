package org.koekepan.herobrine.test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.koekepan.herobrine.ConsoleIO;
import org.koekepan.herobrine.client.ClientEmulator;
import org.koekepan.herobrine.client.HerobrineClient;
import org.koekepan.herobrine.client.packet.PacketListener;
import org.koekepan.herobrine.command.CommandSystem;
import org.koekepan.herobrine.command.HerobrineCommandControl;
import org.spacehq.packetlib.packet.Packet;

public class Test {
	private final static String regex = "^[ ]*waypoint [A-Za-z]+[_]{1}[0-9]{1,4}( [-]{0,1}[0-9]{1,3}){3}[ ]*$";//TODO add ability to specify client
	private static List<ClientEmulator> clients = new LinkedList<ClientEmulator>();
	
	private static String playerName;
	
	public static void main(String[] args) {
		if (args.length < 5) {
			invalidArguments();
			System.exit(-1);
		} 
		
		//get system to wait for VisualVM
		while(true) {
			try {
				Thread.sleep(7000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}

		playerName = args[0];
		String serverName = args[1];
		int serverPort = Integer.parseInt(args[2]);
		int numClients = Integer.parseInt(args[3]);
		String wayPointsFile = args[4];

		ConsoleIO.println("Herobrine client emulator is running...");

		// load waypoints from JSON file

		JSONObject waypoints = readJSONFromFile(wayPointsFile);
		if (waypoints == null) {
			ConsoleIO.println("ERROR: Could not parse JSON waypoints file: " + wayPointsFile);
			System.exit(-1);
		}

		JSONArray pointsArray = (JSONArray) waypoints.get("waypoints");

		int[][] points = new int[pointsArray.size()][3];
		for (int i = 0; i < pointsArray.size(); i++) {
			JSONArray waypoint = (JSONArray) pointsArray.get(i);
			points[i][0] = ((Long) waypoint.get(0)).intValue();
			points[i][1] = ((Long) waypoint.get(1)).intValue();
			points[i][2] = ((Long) waypoint.get(2)).intValue();
		}


		// create clients
		for (int i = 0; i < numClients; i++) {
			String name = playerName+"_"+i;
			clients.add(new HerobrineClient(name, 50, new CommandSystem(new HerobrineCommandControl(points)), serverName, serverPort));
		}

		// connect clients
		for(ClientEmulator client : clients) {

			ConsoleIO.println("Player \"" + client.getName() + "\" connecting to <" + client.getHost() + ":" + client.getPort() + ">");
			client.initializeSession();
			client.connect();
			ConsoleIO.println("Player \"" + client.getName() + "\" connected...");

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}

		ConsoleIO.println("Type \"help\" for a list of commands");
		
		// run until stop
		try {
			String input = "";
			do {
				//ConsoleIO.println("Number of columns: "+clients.get(0).getClientState().getWorldState().countColumns());
				input = ConsoleIO.readLine();
				processInput(input);
			}
			while(!input.equalsIgnoreCase("stop"));
		} catch(IOException e) {
			e.printStackTrace();;
		}

		ConsoleIO.println("Herobrine client emulator is stopping...");

		// disconnect clients
		for(ClientEmulator client : clients) {
			ConsoleIO.println("Player \"" + client.getName() + "\" disconnecting from <" + client.getHost() + ":" + client.getPort() + ">"); // TODO OUTPUT: Player "NAME" disconnecting from <HOST:PORT>
			client.disconnect();
			ConsoleIO.println("Player \"" + client.getName() + "\" disconnected..."); // TODO OUTPUT: Player "NAME" disconnected...
		}

		System.out.println("Press any key to continue...");
		try {
			System.in.read();
		} catch(Exception e) {
			// do nothing
		} 

	}


	private static JSONObject readJSONFromFile(String filename)
	{
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = null;

		try {
			Object obj = parser.parse(new FileReader(filename));
			jsonObject = (JSONObject) obj;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return jsonObject;	
	}

	private static void invalidArguments() {
		ConsoleIO.println("Invalid arguments provided! Valid arguments are:");
		ConsoleIO.println("> [Base player name (String)] [Server host (String)] [Server port (Port)] [Number of clients (int)] [Waypoint file (String)]");
	}

	private static void processInput(String input) {
		if(input.equalsIgnoreCase("help")) {
			synchronized(ConsoleIO.class) {
				ConsoleIO.println("---------------- Commands -----------------");
				ConsoleIO.println("[command] help   				- displays a list of commands");
				ConsoleIO.println("[command] stop   				- shutdowns the client emulator");
				ConsoleIO.println("[command] waypoint [Base player name]_[Client number (from 0)] [x] [y] [z]	- adds a waypoint to the client");
			}
		} else if(input.equalsIgnoreCase("stop")) {
			return;
		} else if (input.matches(regex)){
			//add waypoint to list
			String[] split = input.split(" ");	
			String[] nameSplit = split[1].split("_");
			if (playerName.equalsIgnoreCase(nameSplit[0])) {
				clients.get(Integer.parseInt(nameSplit[1])).addWaypoint(Integer.parseInt(split[2]), Integer.parseInt(split[3]), Integer.parseInt(split[4]));
			} else {
				ConsoleIO.println("Base player name incorrectly entered. Should be in the form of \""+playerName+"_[number]\" (type \"help\" for more information.");
			}
				//TODO add correction string if the incorrect form is given
		} else {
			ConsoleIO.println("Unknown command. Type \"help\" for help");
		}
	}

}