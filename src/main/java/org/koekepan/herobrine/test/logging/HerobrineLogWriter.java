package org.koekepan.herobrine.test.logging;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.koekepan.herobrine.client.packet.PacketListener;
import org.koekepan.herobrine.log.file.LogFileWriter;
import org.koekepan.herobrine.test.logging.logs.ClientPlayerPositionRotationPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerDestroyEntitiesPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerEntityPositionPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerEntityPositionRotationPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerEntityRotationPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerEntityTeleportPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerJoinGamePacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerPlayerPositionRotationPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerSpawnPlayerPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerUpdateTimePacketLog;
import org.spacehq.mc.protocol.packet.ingame.client.player.ClientPlayerPositionRotationPacket;
import org.spacehq.mc.protocol.packet.ingame.server.ServerJoinGamePacket;
import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerDestroyEntitiesPacket;
import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerEntityPositionPacket;
import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerEntityPositionRotationPacket;
import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerEntityRotationPacket;
import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerEntityTeleportPacket;
import org.spacehq.mc.protocol.packet.ingame.server.entity.player.ServerPlayerPositionRotationPacket;
import org.spacehq.mc.protocol.packet.ingame.server.entity.spawn.ServerSpawnPlayerPacket;
import org.spacehq.mc.protocol.packet.ingame.server.world.ServerUpdateTimePacket;
import org.spacehq.packetlib.packet.Packet;

public class HerobrineLogWriter extends LogFileWriter implements PacketListener {

	public HerobrineLogWriter(String filePath) throws FileNotFoundException {
		super(new HerobrineLogHandler(), filePath);
	}

	@Override
	public void packetReceived(Packet packet) {

		try {
			
			if(packet instanceof ServerDestroyEntitiesPacket) { // ServerDestroyEntitiesPacket
				write(new ServerDestroyEntitiesPacketLog((ServerDestroyEntitiesPacket)packet));
			}
			
			if(packet instanceof ServerEntityPositionPacket) { // ServerEntityPositionPacket
				write(new ServerEntityPositionPacketLog((ServerEntityPositionPacket)packet));
			}
			
			if(packet instanceof ServerEntityPositionRotationPacket) { // ServerEntityPositionRotationPacket
				write(new ServerEntityPositionRotationPacketLog((ServerEntityPositionRotationPacket)packet));
			}
			
			if(packet instanceof ServerEntityRotationPacket) { // ServerEntityRotationPacket
				write(new ServerEntityRotationPacketLog((ServerEntityRotationPacket)packet));
			}
			
			if(packet instanceof ServerEntityTeleportPacket) { // ServerEntityTeleportPacket
				write(new ServerEntityTeleportPacketLog((ServerEntityTeleportPacket)packet));
			}
			
			if(packet instanceof ServerJoinGamePacket) { // ServerJoinGamePacket
				write(new ServerJoinGamePacketLog((ServerJoinGamePacket)packet));
			}
			
			if(packet instanceof ServerPlayerPositionRotationPacket) { // ServerPlayerPositionRotationPacket
				write(new ServerPlayerPositionRotationPacketLog((ServerPlayerPositionRotationPacket)packet));
			}
			
			if(packet instanceof ServerSpawnPlayerPacket) { // ServerSpawnPlayerPacket
				write(new ServerSpawnPlayerPacketLog((ServerSpawnPlayerPacket)packet));
			}
			
			if(packet instanceof ServerUpdateTimePacket) { // ServerUpdateTimePacket
				write(new ServerUpdateTimePacketLog((ServerUpdateTimePacket)packet));
			}
			
		} catch(IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void packetSent(Packet packet) {

		try {
			
			if(packet instanceof ClientPlayerPositionRotationPacket) { // ClientPlayerPositionRotationPacket
				write(new ClientPlayerPositionRotationPacketLog((ClientPlayerPositionRotationPacket)packet));
			}
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		
	}

}
