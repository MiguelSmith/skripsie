package org.koekepan.herobrine.test.logging.logs;

import java.io.IOException;

import org.koekepan.herobrine.log.Log;
import org.koekepan.herobrine.log.io.LogInput;
import org.koekepan.herobrine.log.io.LogOutput;
import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerEntityPositionPacket;

public class ServerEntityPositionPacketLog implements Log {

	private int entityId;
	private double x, y, z;
	
	public ServerEntityPositionPacketLog() {
		this(0,0,0,0);
	}
	
	public ServerEntityPositionPacketLog(int entityId, double x, double y, double z) {
		
		this.entityId = entityId;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public ServerEntityPositionPacketLog(ServerEntityPositionPacket packet) {
		this(packet.getEntityId(), packet.getMovementX(), packet.getMovementY(), packet.getMovementZ());
	}
	
	@Override
	public void write(LogOutput out) throws IOException {

		out.writeVarInt(entityId);
		out.writeDouble(x);
		out.writeDouble(y);
		out.writeDouble(z);
	}

	@Override
	public void read(LogInput in) throws IOException {

		entityId = in.readVarInt();
		x = in.readDouble();
		y = in.readDouble();
		z = in.readDouble();
	}

	public int getEntityId() {
		return entityId;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}

}
