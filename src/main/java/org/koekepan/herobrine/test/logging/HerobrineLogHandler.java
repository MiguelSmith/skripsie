package org.koekepan.herobrine.test.logging;

import org.koekepan.herobrine.log.AbstractLogHandler;
import org.koekepan.herobrine.test.logging.logs.ClientPlayerPositionRotationPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerDestroyEntitiesPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerEntityPositionPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerEntityPositionRotationPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerEntityRotationPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerEntityTeleportPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerJoinGamePacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerPlayerPositionRotationPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerSpawnPlayerPacketLog;
import org.koekepan.herobrine.test.logging.logs.ServerUpdateTimePacketLog;

public class HerobrineLogHandler extends AbstractLogHandler {
	
	public HerobrineLogHandler() {
		
		register(0, ClientPlayerPositionRotationPacketLog.class);
		register(1, ServerDestroyEntitiesPacketLog.class);
		register(2, ServerEntityPositionPacketLog.class);
		register(3, ServerEntityPositionRotationPacketLog.class);
		register(4, ServerEntityRotationPacketLog.class);
		register(5, ServerEntityTeleportPacketLog.class);
		register(6, ServerJoinGamePacketLog.class);
		register(7, ServerPlayerPositionRotationPacketLog.class);
		register(8, ServerSpawnPlayerPacketLog.class);
		register(9, ServerUpdateTimePacketLog.class);
	}
}
