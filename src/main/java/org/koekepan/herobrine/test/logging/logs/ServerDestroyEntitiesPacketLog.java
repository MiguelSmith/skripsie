package org.koekepan.herobrine.test.logging.logs;

import java.io.IOException;

import org.koekepan.herobrine.log.Log;
import org.koekepan.herobrine.log.io.LogInput;
import org.koekepan.herobrine.log.io.LogOutput;
import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerDestroyEntitiesPacket;

public class ServerDestroyEntitiesPacketLog implements Log {

	private int[] entityIds;
	
	public ServerDestroyEntitiesPacketLog() {
		entityIds = null;
	}
	
	public ServerDestroyEntitiesPacketLog(int[] entityIds) {
		this.entityIds = entityIds;
	}
	
	public ServerDestroyEntitiesPacketLog(ServerDestroyEntitiesPacket packet) {
		this.entityIds = packet.getEntityIds();
	}
	
	@Override
	public void write(LogOutput out) throws IOException {
		out.writeVarInt(entityIds.length);
		for(int i = 0; i < entityIds.length; i++) {
			out.writeVarInt(entityIds[i]);
		}
	}

	@Override
	public void read(LogInput in) throws IOException {
		entityIds = new int[in.readVarInt()];
		for(int i = 0; i < entityIds.length; i++) {
			entityIds[i] = in.readVarInt();
		}
	}
	
	public int[] getEntityIds() {
		return entityIds;
	}

}
