package org.koekepan.herobrine.test.logging.logs;

import java.io.IOException;

import org.koekepan.herobrine.log.Log;
import org.koekepan.herobrine.log.io.LogInput;
import org.koekepan.herobrine.log.io.LogOutput;
import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerEntityPositionRotationPacket;

public class ServerEntityPositionRotationPacketLog implements Log {

	private int entityId;
	private double x, y, z;
	private double yaw, pitch;
	
	public ServerEntityPositionRotationPacketLog() {
		this(0,0,0,0,0,0);
	}
	
	public ServerEntityPositionRotationPacketLog(int entityId, double x, double y, double z, double yaw, double pitch) {
		
		this.entityId = entityId;
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
	}
	
	public ServerEntityPositionRotationPacketLog(ServerEntityPositionRotationPacket packet) {
		this(packet.getEntityId(), packet.getMovementX(), packet.getMovementY(), packet.getMovementZ(), packet.getYaw(), packet.getPitch());
	}
	
	@Override
	public void write(LogOutput out) throws IOException {

		out.writeVarInt(entityId);
		out.writeDouble(x);
		out.writeDouble(y);
		out.writeDouble(z);
		out.writeDouble(yaw);
		out.writeDouble(pitch);
	}

	@Override
	public void read(LogInput in) throws IOException {

		entityId = in.readVarInt();
		x = in.readDouble();
		y = in.readDouble();
		z = in.readDouble();
		yaw = in.readDouble();
		pitch = in.readDouble();
	}

	public int getEntityId() {
		return entityId;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public double getYaw() {
		return yaw;
	}
	
	public double getPitch() {
		return pitch;
	}

}
