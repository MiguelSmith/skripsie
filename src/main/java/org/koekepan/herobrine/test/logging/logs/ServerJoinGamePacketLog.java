package org.koekepan.herobrine.test.logging.logs;

import java.io.IOException;

import org.koekepan.herobrine.log.Log;
import org.koekepan.herobrine.log.io.LogInput;
import org.koekepan.herobrine.log.io.LogOutput;
import org.spacehq.mc.protocol.packet.ingame.server.ServerJoinGamePacket;

public class ServerJoinGamePacketLog implements Log {

	private int entityId;
	
	public ServerJoinGamePacketLog() {
		entityId = 0;
	}
	
	public ServerJoinGamePacketLog(int entityId) {
		this.entityId = entityId;
	}
	
	public ServerJoinGamePacketLog(ServerJoinGamePacket packet) {
		entityId = packet.getEntityId();
	}
	
	@Override
	public void write(LogOutput out) throws IOException {
		out.writeVarInt(entityId);
	}

	@Override
	public void read(LogInput in) throws IOException {
		entityId = in.readVarInt();
	}
	
	public int getEntityId() {
		return entityId;
	}

}
