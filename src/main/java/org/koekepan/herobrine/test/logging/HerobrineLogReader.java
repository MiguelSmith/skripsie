package org.koekepan.herobrine.test.logging;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.koekepan.herobrine.log.Log;
import org.koekepan.herobrine.log.file.LogFileReader;

public class HerobrineLogReader extends LogFileReader {

	private List<Log> logs = new ArrayList<Log>();
	
	public HerobrineLogReader(String filePath) throws FileNotFoundException {
		super(new HerobrineLogHandler(), filePath);
		
		Log log = null;
		try {
			while((log = read()) != null) {
				logs.add(log);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Log[] getLogs() {
		return logs.toArray(new Log[logs.size()]);
	}

}
