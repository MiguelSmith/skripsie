package org.koekepan.herobrine.test.logging.logs;

import java.io.IOException;

import org.koekepan.herobrine.log.Log;
import org.koekepan.herobrine.log.io.LogInput;
import org.koekepan.herobrine.log.io.LogOutput;
import org.spacehq.mc.protocol.packet.ingame.server.entity.ServerEntityRotationPacket;

public class ServerEntityRotationPacketLog implements Log {

	private int entityId;
	private double yaw, pitch;
	
	public ServerEntityRotationPacketLog() {
		this(0,0,0);
	}
	
	public ServerEntityRotationPacketLog(int entityId, double yaw, double pitch) {
		
		this.entityId = entityId;
		this.yaw = yaw;
		this.pitch = pitch;
	}
	
	public ServerEntityRotationPacketLog(ServerEntityRotationPacket packet) {
		this(packet.getEntityId(), packet.getYaw(), packet.getPitch());
	}
	
	@Override
	public void write(LogOutput out) throws IOException {

		out.writeVarInt(entityId);
		out.writeDouble(yaw);
		out.writeDouble(pitch);
	}

	@Override
	public void read(LogInput in) throws IOException {

		entityId = in.readVarInt();
		yaw = in.readDouble();
		pitch = in.readDouble();
	}

	public int getEntityId() {
		return entityId;
	}
	
	public double getYaw() {
		return yaw;
	}
	
	public double getPitch() {
		return pitch;
	}

}
