package org.koekepan.herobrine.test.logging.logs;

import java.io.IOException;

import org.koekepan.herobrine.log.Log;
import org.koekepan.herobrine.log.io.LogInput;
import org.koekepan.herobrine.log.io.LogOutput;
import org.spacehq.mc.protocol.packet.ingame.server.world.ServerUpdateTimePacket;

public class ServerUpdateTimePacketLog implements Log {

	private long dayTime, worldAge;
	
	public ServerUpdateTimePacketLog() {
		this(0,0);
	}
	
	public ServerUpdateTimePacketLog(long dayTime, long worldAge) {
		this.dayTime = dayTime;
		this.worldAge = worldAge;
	}
	
	public ServerUpdateTimePacketLog(ServerUpdateTimePacket packet) {
		this(packet.getTime(), packet.getWorldAge());
	}
	
	@Override
	public void write(LogOutput out) throws IOException {
		out.writeVarLong(dayTime);
		out.writeVarLong(worldAge);
	}

	@Override
	public void read(LogInput in) throws IOException {
		dayTime = in.readVarLong();
		worldAge = in.readVarLong();
	}
	
	public long getDayTime() {
		return dayTime;
	}
	
	public long getWorldAge() {
		return worldAge;
	}

}
