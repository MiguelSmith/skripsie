package org.koekepan.herobrine.test.logging.logs;

import java.io.IOException;

import org.koekepan.herobrine.log.Log;
import org.koekepan.herobrine.log.io.LogInput;
import org.koekepan.herobrine.log.io.LogOutput;
import org.spacehq.mc.protocol.packet.ingame.client.player.ClientPlayerPositionRotationPacket;

public class ClientPlayerPositionRotationPacketLog implements Log {

	private double x, y, z;
	private double yaw, pitch;
	
	public ClientPlayerPositionRotationPacketLog() {
		this(0,0,0,0,0);
	}
	
	public ClientPlayerPositionRotationPacketLog(double x, double y, double z, double yaw, double pitch) {
		
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
	}
	
	public ClientPlayerPositionRotationPacketLog(ClientPlayerPositionRotationPacket packet) {
		this(packet.getX(), packet.getY(), packet.getZ(), packet.getYaw(), packet.getPitch());
	}
	
	@Override
	public void write(LogOutput out) throws IOException {
		
		out.writeDouble(x);
		out.writeDouble(y);
		out.writeDouble(z);
		out.writeDouble(yaw);
		out.writeDouble(pitch);
	}

	@Override
	public void read(LogInput in) throws IOException {

		x = in.readDouble();
		y = in.readDouble();
		z = in.readDouble();
		yaw = in.readDouble();
		pitch = in.readDouble();
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public double getYaw() {
		return yaw;
	}
	
	public double getPitch() {
		return pitch;
	}

}
